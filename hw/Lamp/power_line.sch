EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 622D42C1
P 1200 2400
F 0 "J1" H 1118 2075 50  0000 C CNN
F 1 "PWR_IN" H 1118 2166 50  0000 C CNN
F 2 "" H 1200 2400 50  0001 C CNN
F 3 "~" H 1200 2400 50  0001 C CNN
	1    1200 2400
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C1
U 1 1 622D4E70
P 1650 2100
F 0 "C1" H 1532 2054 50  0000 R CNN
F 1 "220uF/10V" H 1532 2145 50  0000 R CNN
F 2 "" H 1688 1950 50  0001 C CNN
F 3 "~" H 1650 2100 50  0001 C CNN
	1    1650 2100
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 622D790B
P 1450 2450
F 0 "#PWR01" H 1450 2200 50  0001 C CNN
F 1 "GND" H 1455 2277 50  0000 C CNN
F 2 "" H 1450 2450 50  0001 C CNN
F 3 "" H 1450 2450 50  0001 C CNN
	1    1450 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 2400 1450 2400
Wire Wire Line
	1450 2400 1450 2450
Wire Wire Line
	1400 2300 1650 2300
Wire Wire Line
	1650 2300 1650 2250
$Comp
L power:GND #PWR02
U 1 1 622D7F5B
P 1650 1950
F 0 "#PWR02" H 1650 1700 50  0001 C CNN
F 1 "GND" H 1655 1777 50  0000 C CNN
F 2 "" H 1650 1950 50  0001 C CNN
F 3 "" H 1650 1950 50  0001 C CNN
	1    1650 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 2300 2350 2300
Connection ~ 1650 2300
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 622DA3AA
P 2550 2400
F 0 "J2" H 2630 2392 50  0000 L CNN
F 1 "MCU board" H 2630 2301 50  0000 L CNN
F 2 "" H 2550 2400 50  0001 C CNN
F 3 "~" H 2550 2400 50  0001 C CNN
	1    2550 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 622DB4E7
P 2550 2800
F 0 "J3" H 2630 2792 50  0000 L CNN
F 1 "LED board" H 2630 2701 50  0000 L CNN
F 2 "" H 2550 2800 50  0001 C CNN
F 3 "~" H 2550 2800 50  0001 C CNN
	1    2550 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 2900 1650 2900
Wire Wire Line
	1650 2900 1650 2300
Wire Wire Line
	2350 2800 2300 2800
Wire Wire Line
	2300 2800 2300 2600
Wire Wire Line
	2300 2600 2350 2600
Wire Wire Line
	1450 2400 2300 2400
Connection ~ 1450 2400
Wire Wire Line
	2350 2500 2300 2500
Wire Wire Line
	2300 2500 2300 2400
Connection ~ 2300 2400
Wire Wire Line
	2300 2400 2350 2400
Text Label 2300 2800 2    50   ~ 0
LED_MINUS
Text Label 2300 2500 2    50   ~ 0
PWR_GND
Text Notes 1800 1900 0    50   ~ 0
Condensator reduce high pitch\nnoise sound caused by alternating\ncurrent in wires (magnetic coupling).\nIdeally, wire for LED board shall be\nseparate wire connected to this capacitor.
Text Notes 3100 2750 0    50   ~ 0
On MCU board there are\nseparated grounds for power\nline and MCU. Do not connect\nthem on MCU board. Keep it\nseparated
$EndSCHEMATC
