/**
 * @file
 * @author Martin Stejskal
 * @brief Main user function - mainly for creating tasks
 */
// ===============================| Includes |================================
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <soc/cpu.h>
#include <string.h>

#include "app.h"
#include "cfg.h"
#include "log_system.h"
#include "ui_service_core.h"
#include "ui_service_user.h"
// ================================| Defines |================================
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================
/**
 * @brief Tag for logging system
 */
static char *tag = "main";
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void _ui_service_core_rtos_task(void *pv_args);

#if SHOW_STACK_USAGE
/**
 * @brief Task which periodically print stack usage
 * @param pvParameters Parameters - not used actually
 */
static void _show_stack_usage(void *pv_args);
#endif
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
static void _delay_ms(const uint16_t u16_delay_ms);
// =========================| High level functions |==========================
void app_main(void) {
  LOGI(tag, "Steampunk lamp!");

  xTaskCreate(app_task_rtos, "App", 4 * 1024, NULL, APP_TASK_PRIORITY + 1,
              NULL);

  xTaskCreate(_ui_service_core_rtos_task, "Serv UI", 4 * 1024, NULL,
              UI_SERVICE_CORE_TASK_PRIORITY, NULL);

#if SHOW_STACK_USAGE
  xTaskCreate(_show_stack_usage, "task usage", 4 * 1024, NULL, tskIDLE_PRIORITY,
              NULL);
#endif
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static void _ui_service_core_rtos_task(void *pv_args) {
  // Configure task
  ts_usc_config_args s_args = {
      .pf_read_itf_cb = usu_uart_read,
      .pf_write_itf_cb = usu_uart_write,
      .pf_get_password_cb = NULL,
      .pf_get_list_of_cmds_cb = ui_service_user_get_list_of_cmds,
      .pf_get_num_of_cmds_cb = ui_service_user_get_num_of_cmds,

      // No need to notify application layer when entering/leaving service mode
      .pf_enter_service_mode_cb = 0,
      .pf_enter_service_mode_is_ready_cb = 0,
      .pf_leave_service_mode_cb = 0,
      .pf_leave_service_mode_is_ready_cb = 0,

      .pf_delay_ms_cb = _delay_ms,
  };

  te_usc_error_code e_err_code = usc_configure(&s_args);

  if (e_err_code) {
    LOGE(tag, "Configuration of UI service failed: %s",
         usc_error_code_to_string(e_err_code));
    vTaskDelete(0);
    return;
  }

  // Initialize service UART
  e_err_code = usu_uart_init();
  if (e_err_code) {
    LOGE(tag, "Service UART initialization failed");
    vTaskDelete(0);
    return;
  } else {
    // Write initial message
    const char *pac_welcome_message = "\n\nSteampunk lamp alive!\n\n";
    usu_uart_write((uint8_t *)pac_welcome_message, strlen(pac_welcome_message),
                   UINT16_MAX);
  }

  // Before offering service UI, let the dust settle in order to avoid further
  // delays due to too many requests to UART interfaces at the initial stage
  _delay_ms(3500);

  // There can be some additional logic, which allow to exit from loop. Maybe
  // in the future...
  while (1) {
    usc_task_exec();

    _delay_ms(UI_SERVICE_TASK_RECOMMENDED_PERIOD_MS);
  }

  // Deinitialize service UART
  e_err_code = usu_uart_deinit();
  if (e_err_code) {
    LOGE(tag, "Service UART deinitialization failed");
    vTaskDelete(0);
    return;
  }

  // Quit task gracefully
  vTaskDelete(0);
}

#if SHOW_STACK_USAGE
static const char *tag_stack_usage = "Stack usage";

static void _show_stack_usage(void *pv_args) {
  UBaseType_t u_num_of_tasks;
  TaskStatus_t *px_task_status_array;
  uint32_t u32_total_run_time;

  while (1) {
    vTaskDelay(5000 / portTICK_PERIOD_MS);
    LOGI(tag_stack_usage, "=================================");

    // Take a snapshot of the number of tasks in case it changes while this
    // function is executing.
    u_num_of_tasks = uxTaskGetNumberOfTasks();
    // Allocate a TaskStatus_t structure for each task.  An array could be
    // allocated statically at compile time.
    px_task_status_array = pvPortMalloc(u_num_of_tasks * sizeof(TaskStatus_t));

    // Pointer should not be empty, but just in case check it
    if (px_task_status_array) {
      uxTaskGetSystemState(px_task_status_array, u_num_of_tasks,
                           &u32_total_run_time);
      for (int iTskCnt = 0; iTskCnt < u_num_of_tasks; iTskCnt++) {
        LOGI(tag_stack_usage, "%s | Free Bytes: %d",
             px_task_status_array[iTskCnt].pcTaskName,
             px_task_status_array[iTskCnt].usStackHighWaterMark);
      }
    }

    // The array is no longer needed, free the memory it consumes.
    vPortFree(px_task_status_array);
    LOGI(tag_stack_usage, "=================================");
  }
}
#endif
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static void _delay_ms(const uint16_t u16_delay_ms) {
  DELAY_MS((uint32_t)u16_delay_ms);
}
