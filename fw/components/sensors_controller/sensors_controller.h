/**
 * @file
 * @author Martin Stejskal
 * @brief Handle sensor processing from magnetometer and accelerometer
 */
#ifndef __SENSORS_CONTROLLER_H__
#define __SENSORS_CONTROLLER_H__
// ===============================| Includes |================================
#include <stdint.h>

#include "events.h"
// ================================| Defines |================================
/**
 * @brief Define how often will be data read from magnetic sensor in ms
 */
#define SENSORS_CNTRLR_MAG_PERIOD_MS (100)

/**
 * @brief Define how often will be data read from temperature sensor in ms
 */
#define SENSORS_CNTRLR_TEMP_PERIOD_MS (5000)

/**
 * @brief Define how many times should be functionality retried before give up
 *
 * In the initialization function is checked if required sensor feature is
 * available. If for some reason that sensor ill return error codes, internal
 * logic will re-try few more times (value below). If reading from sensor fails
 * more times that this threshold, it will be considered that feature is not
 * available anymore (sensor disconnected/burned out/etc.) and task will give
 * up further reading and exit itself.
 */
#define SENSORS_CNTRLR_RETRY_IF_FAIL (10)
// ============================| Default values |=============================
/**
 * @brief Define default threshold when overheating event will be sent
 *
 * Once detected overheating threshold, this will trigger sending event via
 * callback function. Value is in degrees of Celsius.
 */
#define SENSORS_CNTRLR_DEFAULT_TEMP_THRESHOLD_DEG (45)

/**
 * @brief Define default "dead zone" threshold in degrees
 *
 * When magnet is moving a bit or due to magnetometer inaccuracy, angle can
 * bit drifting. Therefore this threshold helps to ignore changes smaller than
 * this. Value is in degrees. This value prevents unwanted increasing or
 * decreasing light intensity due to angle drift.
 */
#define SENSORS_CNTRLR_DEFAULT_MAG_DEADZONE_DEG (15)

/**
 * @brief Define default "zero position" toleration in degrees
 *
 * When rotating magnet back to initial position (when no intensity is
 * changed), typically user will not put it on exactly same place. It would be
 * annoying and impractical trying to find that exact position. Therefore here
 * is possible to define default tolerance when rotating magnet back to it's
 * initial position.
 */
#define SENSORS_CNTRLR_DEFAULT_ZERO_POS_TOL_DEG (25)

/**
 * @brief Default threshold for detecting magnet in uT
 *
 * Since everywhere around us is natural magnetic field, which is curved due
 * to ferromagnetic material around, there have to be defined some reasonable
 * threshold when we can say "yes, this is magnet" and "nope, no magnet here".
 * Every magnetometer can show values bit differently (different offset),
 * so even this should be considered.
 * Good way to get this value is to readout magnetic field strength inside
 * device without magnet and then slowly move magnet toward device. Once
 * numbers will be significantly different, note that value down.
 *
 */
#define SENSORS_CNTRLR_DEFAULT_MAGNET_DETECTED_UT (300)

/**
 * @brief How many times have to be angle "stable" to acknowledge "zero
 *        position"
 *
 * When approaching magnet to the sensor, at some point magnet will be
 * detected, but it might not have to be "zero position", since user can hold
 * magnet in and and it might bit rotate. For these cases, algorithm needs to
 * confirm that position is "fixed" and it is possible to do via checking
 * angle difference. Higher number means higher certainty, but it will delay
 * whole process, due to requiring more samples.
 */
#define SENSORS_CNTRLR_DEFAULT_MAG_APPRCH_STABLE_ANGLE_CNT (10)
// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Initialize "sensor controller" module
 *
 * @param pf_event_cb Pointer to function, which will be called when some event
 *        occurs.
 */
void sensors_cntrlr_init(const tp_event_cb pf_event_cb);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Get temperature threshold value in degrees of Celsius
 *
 * When temperature is reached, event @ref EVENT_TYPE_OVERHEATING is sent
 *
 * @return Temperature threshold in degrees of Celsius
 */
int8_t sensors_cntrlr_get_temp_thrshld(void);

/**
 * @brief Set temperature threshold value in degrees of Celsius
 *
 * When temperature is reached, event @ref EVENT_TYPE_OVERHEATING is sent
 *
 * @param i8_temperature_threshold Temperature threshold value in degrees of
 *        Celsius
 */
void sensors_cntrlr_set_temp_thrshld(int8_t i8_temperature_threshold);

/**
 * @brief Get current temperature from sensors
 *
 * @return Temperature in degrees of Celsius. If temperature sensor is not
 *         available, it will return -127.
 */
int8_t sensors_cntrlr_get_temp(void);

/**
 * @brief Get threshold for detecting magnet in uT
 *
 * This threshold helps to detect whether magnet is close to sensors or not
 *
 * @return Threshold in uT
 */
int32_t sensors_cntrlr_get_mag_threshold_ut(void);

/**
 * @brief Set threshold for detecting magnet in uT
 *
 * This threshold helps to detect whether magnet is close to sensors or not
 *
 * @param u32_mag_threshold_ut Threshold in uT. Practical range is around few
 *                             hundreds, but feel free to experiment if needed
 */
void sensors_cntrl_set_mag_threshold_ut(const uint32_t u32_mag_threshold_ut);

/**
 * @brief
 *
 * @return
 */
int32_t sensors_cntrlr_get_mag_amplitude_ut(void);

uint8_t sensors_cntrlr_get_stable_angle_cnt(void);
void sensors_cntrlr_set_stable_angle_cnt(const uint8_t u8_stable_angle_cnt);

uint16_t sensors_cntrlr_get_mag_deadzone_deg(void);
void sensors_cntrlr_set_mag_deadzone_deg(const uint16_t u16_mag_deadzone_deg);

uint16_t sensors_cntrlr_get_mag_zero_pos_tol_deg(void);
void sensors_cntrlr_set_mag_zero_pos_tol_deg(
    const uint16_t u16_mag_zero_pos_tol_deg);

#endif  // __SENSORS_CONTROLLER_H__
