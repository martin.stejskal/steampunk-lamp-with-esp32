/**
 * @file
 * @author Martin Stejskal
 * @brief Handle sensor processing from magnetometer and accelerometer
 */
// ===============================| Includes |================================
#include "sensors_controller.h"

#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>
#include <stdbool.h>
#include <string.h>

#include "log_system.h"
#include "math_fast.h"
#include "math_vector.h"
#include "nvs_settings.h"
#include "sensors.h"
#include "sensors_common.h"
// ================================| Defines |================================
#define _TEMP_THRESHOLD_MAX_DEG (127)
#define _TEMP_THRESHOLD_MIN_DEG (-128)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================
#if SENSORS_CNTRLR_DEFAULT_TEMP_THRESHOLD_DEG > _TEMP_THRESHOLD_MAX_DEG
#error "Hit upper limit for SENSORS_CNTRLR_DEFAULT_TEMP_THRESHOLD_DEG"
#endif

#if SENSORS_CNTRLR_DEFAULT_TEMP_THRESHOLD_DEG < _TEMP_THRESHOLD_MIN_DEG
#error "Hit lower limit for SENSORS_CNTRLR_DEFAULT_TEMP_THRESHOLD_DEG"
#endif
// =======================| Structures, enumerations |========================
typedef struct {
  int32_t i32_angle_deg;
  uint32_t u32_timestamp_ms;
  int32_t i32_amplitude_ut;
} ts_mag_angle_amplitude_timestamp;

typedef struct {
  // Temperature from sensor is multiplied by 10 -> better to use 16 bit
  int16_t i16_temp_threshold_10x;

  // Threshold when magnet is detected and when not. Value in uT
  int32_t i32_mag_detected_threshold_ut;

  /* How many times angle have to be within limits in order to say that zero
   * position is reached
   */
  uint8_t u8_mag_approach_stable_angle_cnt;

  // Dead zone for magnetometer when rotating
  int32_t i32_mag_deadzone_deg;

  // Magnet initial "zero position" tolerance in degrees
  int32_t i32_mag_zero_pos_tol_deg;
} ts_nvs_cfg;

typedef struct {
  // Configuration structure stored also in NVS
  ts_nvs_cfg s_cfg;

  // When magnet is detected, initial intensity and angle have to be stored
  ts_mag_angle_amplitude_timestamp s_mag_init_data;

  // Last measured temperature is stored here
  int8_t i8_last_measured_temperature;

  // Last measured magnetic field amplitude
  int32_t i32_last_measured_mag_amplitude;

  // Deal with access to sensors via multiple tasks
  SemaphoreHandle_t s_mutex;
} ts_sensors_cntrlr_vars;

typedef enum {
  STATE_MAGNET_NOT_PRESENT,
  STATE_MAGNET_APPROACHING,
  STATE_MAGNET_ZERO_POSITION,
  STATE_MAGNET_ACTION_POSITION,
} te_states;
// ===========================| Global variables |============================
static char *tag = "Sensors ctrlr";

static ts_sensors_cntrlr_vars ms_sensors_cntrl_var = {
    .s_cfg.i16_temp_threshold_10x =
        SENSORS_CNTRLR_DEFAULT_TEMP_THRESHOLD_DEG * 10,
    .s_cfg.i32_mag_detected_threshold_ut =
        SENSORS_CNTRLR_DEFAULT_MAGNET_DETECTED_UT,
    .s_cfg.u8_mag_approach_stable_angle_cnt =
        SENSORS_CNTRLR_DEFAULT_MAG_APPRCH_STABLE_ANGLE_CNT,

    .s_cfg.i32_mag_deadzone_deg = SENSORS_CNTRLR_DEFAULT_MAG_DEADZONE_DEG,

    .s_cfg.i32_mag_zero_pos_tol_deg = SENSORS_CNTRLR_DEFAULT_ZERO_POS_TOL_DEG,

    // Set super low temperature for case that temperature sensor is not
    // installed. In that case upper layer can recognize obviously low
    // temperature and assume that value is not valid
    .i8_last_measured_temperature = _TEMP_THRESHOLD_MIN_DEG,
};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void _mag_task_rtos(void *pv_args);
static void _temp_task_rtos(void *pv_args);
// ==============| Internal function prototypes: middle level |===============
static te_states _state_magnet_not_present(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    const tp_event_cb pf_event_cb);

static te_states _state_magnet_approaching(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    const tp_event_cb pf_event_cb);

static te_states _state_magnet_zero_position(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    const tp_event_cb pf_event_cb);
static te_states _state_magnet_action_position(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    const tp_event_cb pf_event_cb);

static e_sensor_error _get_mag_angle_amplitude_timestamp(
    ts_mag_angle_amplitude_timestamp *ps_angle_amplitude_timestamp);
static void _load_settings_from_nvs(ts_nvs_cfg *ps_var);
static void _save_settings_to_nvs(ts_nvs_cfg *ps_var);
// ================| Internal function prototypes: low level |================
static void _get_angle_diff(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    int32_t *pi32_angle_diff, int32_t *pi32_angle_diff_abs);
/**
 * @brief Read data from sensor
 *
 * If something goes wrong, it retry reading from sensor. If it will keep
 * failing, it return SENSOR_UNEXPECTED_EXTERNAL_CONDITIONS
 *
 * @param e_feature Requested feature. Only magnetometer and temperature is
 *                  expected
 * @param ps_data Measured data will be stored here.
 * @return SENSOR_UNEXPECTED_EXTERNAL_CONDITIONS if reading from sensors
 *         multiple times. SENSOR_OK otherwise.
 */
static e_sensor_error _get_sensor_data(const te_sensor_feature e_feature,
                                       ts_sensor_utils_data *ps_data);
// =========================| High level functions |==========================
void sensors_cntrlr_init(const tp_event_cb pf_event_cb) {
  // Make compiler happy when log system is disabled
  (void)tag;

  e_sensor_error e_sens_err = sensors_initialize();

  if (e_sens_err == SENSOR_OK) {
    // Initialization OK. Create mutex in order to avoid parallel reading from
    // sensor module
    ms_sensors_cntrl_var.s_mutex = xSemaphoreCreateMutex();
    if (!ms_sensors_cntrl_var.s_mutex) {
      LOGE(tag, "Creating mutex failed");
      return;
    }
    // Currently, sensor resources are free
    xSemaphoreGive(ms_sensors_cntrl_var.s_mutex);

    // Check if required features are enabled and if yes, execute separate task

    if (sensor_is_available(SENSOR_FEATURE_MAGENETOMETER)) {
      LOGI(tag, "Magnetometer support enabled");
      xTaskCreate(_mag_task_rtos, "Mag sens", 4 * 1024, pf_event_cb,
                  SENSORS_CNTRLR_MAG_TASK_PRIORITY, NULL);

    } else {
      LOGW(tag, "Magnetometer support not available");
    }

    if (sensor_is_available(SENSOR_FEATURE_TEMPERATURE)) {
      LOGI(tag, "Temperature support enabled");
      xTaskCreate(_temp_task_rtos, "Temp sens", 4 * 1024, pf_event_cb,
                  SENSORS_CNTRLR_TEMP_TASK_PRIORITY, NULL);

    } else {
      LOGW(tag, "Temperature support not available");
    }

    _load_settings_from_nvs(&ms_sensors_cntrl_var.s_cfg);

  } else {
    // Can not initialize sensors at all. Exiting task now
    LOGE(tag, "Sensor initialization failed: %s",
         sensor_error_code_to_str(e_sens_err));
  }
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
int8_t sensors_cntrlr_get_temp_thrshld(void) {
  /* Value used for processing is 10x degrees of Celsius in order to get at
   * least 1 decimal point while using integer (avoid using float). Need to
   * convert this value to 1x degrees of Celsius
   */
  int16_t i16_deg_1x = ms_sensors_cntrl_var.s_cfg.i16_temp_threshold_10x / 10;

  assert(i16_deg_1x <= _TEMP_THRESHOLD_MAX_DEG);
  assert(i16_deg_1x >= _TEMP_THRESHOLD_MIN_DEG);

  return (int8_t)i16_deg_1x;
}

void sensors_cntrlr_set_temp_thrshld(int8_t i8_temperature_threshold) {
  // Working with 10x of degrees Celsius. It will always fit here
  ms_sensors_cntrl_var.s_cfg.i16_temp_threshold_10x =
      (int16_t)i8_temperature_threshold * 10;

  // Store settings in NVS in order to keep changes
  _save_settings_to_nvs(&ms_sensors_cntrl_var.s_cfg);
}

int8_t sensors_cntrlr_get_temp(void) {
  return ms_sensors_cntrl_var.i8_last_measured_temperature;
}

int32_t sensors_cntrlr_get_mag_threshold_ut(void) {
  return ms_sensors_cntrl_var.s_cfg.i32_mag_detected_threshold_ut;
}

void sensors_cntrl_set_mag_threshold_ut(const uint32_t u32_mag_threshold_ut) {
  assert(u32_mag_threshold_ut <= INT32_MAX);
  // There should not be reason to set so high value. Using signed integer,
  // since other parts/functions are simply using signed, so this makes things
  // easier

  ms_sensors_cntrl_var.s_cfg.i32_mag_detected_threshold_ut =
      u32_mag_threshold_ut;

  // Store settings in NVS in order to keep changes
  _save_settings_to_nvs(&ms_sensors_cntrl_var.s_cfg);
}

int32_t sensors_cntrlr_get_mag_amplitude_ut(void) {
  return ms_sensors_cntrl_var.i32_last_measured_mag_amplitude;
}

uint8_t sensors_cntrlr_get_stable_angle_cnt(void) {
  return ms_sensors_cntrl_var.s_cfg.u8_mag_approach_stable_angle_cnt;
}

void sensors_cntrlr_set_stable_angle_cnt(const uint8_t u8_stable_angle_cnt) {
  ms_sensors_cntrl_var.s_cfg.u8_mag_approach_stable_angle_cnt =
      u8_stable_angle_cnt;

  // Store settings in NVS in order to keep changes
  _save_settings_to_nvs(&ms_sensors_cntrl_var.s_cfg);
}

uint16_t sensors_cntrlr_get_mag_deadzone_deg(void) {
  assert(ms_sensors_cntrl_var.s_cfg.i32_mag_deadzone_deg >= 0);
  assert(ms_sensors_cntrl_var.s_cfg.i32_mag_deadzone_deg <= UINT16_MAX);

  return (uint16_t)ms_sensors_cntrl_var.s_cfg.i32_mag_deadzone_deg;
}

void sensors_cntrlr_set_mag_deadzone_deg(const uint16_t u16_mag_deadzone_deg) {
  ms_sensors_cntrl_var.s_cfg.i32_mag_deadzone_deg =
      (int32_t)u16_mag_deadzone_deg;

  // Store settings in NVS in order to keep changes
  _save_settings_to_nvs(&ms_sensors_cntrl_var.s_cfg);
}

uint16_t sensors_cntrlr_get_mag_zero_pos_tol_deg(void) {
  assert(ms_sensors_cntrl_var.s_cfg.i32_mag_zero_pos_tol_deg >= 0);
  assert(ms_sensors_cntrl_var.s_cfg.i32_mag_zero_pos_tol_deg <= UINT16_MAX);

  return (uint16_t)ms_sensors_cntrl_var.s_cfg.i32_mag_zero_pos_tol_deg;
}

void sensors_cntrlr_set_mag_zero_pos_tol_deg(
    const uint16_t u16_mag_zero_pos_tol_deg) {
  ms_sensors_cntrl_var.s_cfg.i32_mag_zero_pos_tol_deg =
      (int32_t)u16_mag_zero_pos_tol_deg;

  // Store settings in NVS in order to keep changes
  _save_settings_to_nvs(&ms_sensors_cntrl_var.s_cfg);
}
// ====================| Internal functions: high level |=====================
static void _mag_task_rtos(void *pv_args) {
  e_sensor_error e_err_code = SENSOR_FAIL;

  // Callback function
  const tp_event_cb pf_event_cb = (tp_event_cb)pv_args;

  /* Variable for state machine. Basically there are 2 states:
   *   * Magnet present
   *   * Magnet not present
   *
   * Assume that magnet is not present, but state machine will figure this out
   * safely anyway. It is considered that this is "safe state".
   */
  te_states e_state = STATE_MAGNET_NOT_PRESENT;

  /* When state changes, do not apply delay, in order to process things
   * smoothly
   */
  te_states e_old_state = e_state;

  // Get first reading, so recognize if magnet is present, or not
  ts_mag_angle_amplitude_timestamp s_current_data = {0};

  bool b_keep_it_running = true;

  while (b_keep_it_running) {
    e_err_code = _get_mag_angle_amplitude_timestamp(&s_current_data);

    // Check if there was any problem
    if (e_err_code == SENSOR_UNEXPECTED_EXTERNAL_CONDITIONS) {
      // Jump out from loop and terminate task
      b_keep_it_running = false;
    } else if (e_err_code) {
      LOGW(tag, "Sensor reported error: %s",
           sensor_error_code_to_str(e_err_code));
    } else {
      // Error code is OK
      switch (e_state) {
        case STATE_MAGNET_NOT_PRESENT:
          e_state = _state_magnet_not_present(&s_current_data, pf_event_cb);
          break;

        case STATE_MAGNET_APPROACHING:
          e_state = _state_magnet_approaching(&s_current_data, pf_event_cb);
          break;

        case STATE_MAGNET_ZERO_POSITION:
          e_state = _state_magnet_zero_position(&s_current_data, pf_event_cb);
          break;

        case STATE_MAGNET_ACTION_POSITION:
          e_state = _state_magnet_action_position(&s_current_data, pf_event_cb);
          break;

        default:
          // This should not happen
          assert(0);
          b_keep_it_running = false;
      }

      // Sleep for a while if state is same
      if (e_state == e_old_state) {
        sensor_delay_ms(SENSORS_CNTRLR_MAG_PERIOD_MS);
      }

      e_old_state = e_state;
    }
  }

  // Exit task gracefully
  vTaskDelete(0);
}

static void _temp_task_rtos(void *pv_args) {
  e_sensor_error e_err_code = SENSOR_FAIL;

  // Data from sensor
  ts_sensor_utils_data s_measured_data = {0};

  // Callback function
  const tp_event_cb pf_event_cb = (tp_event_cb)pv_args;

  while (1) {
    e_err_code = _get_sensor_data(SENSOR_FEATURE_TEMPERATURE, &s_measured_data);

    // Check if there was any problem
    if (e_err_code == SENSOR_UNEXPECTED_EXTERNAL_CONDITIONS) {
      // Jump out from loop and terminate task
      break;
    }

    // Check if threshold was reached and if yes, execute callback
    if (s_measured_data.u_d.i16 >
        ms_sensors_cntrl_var.s_cfg.i16_temp_threshold_10x) {
      LOGW(tag, "%s", sensor_data_to_str(&s_measured_data));
      pf_event_cb(EVENT_TYPE_OVERHEATING);
    }

    // Store latest measured value. Temperature value is 10x bigger, so in order
    // to get degrees, divide by 10
    ms_sensors_cntrl_var.i8_last_measured_temperature =
        (s_measured_data.u_d.i16 / 10);

    // Sleep for a while
    sensor_delay_ms(SENSORS_CNTRLR_TEMP_PERIOD_MS);
  }

  // Exit task gracefully
  vTaskDelete(0);
}

// ===================| Internal functions: middle level |====================
static te_states _state_magnet_not_present(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    const tp_event_cb pf_event_cb) {
  te_states e_state = STATE_MAGNET_NOT_PRESENT;

  if (ps_data->i32_amplitude_ut >
      ms_sensors_cntrl_var.s_cfg.i32_mag_detected_threshold_ut) {
    // Magnet is present -> need to change state in order to properly process
    e_state = STATE_MAGNET_APPROACHING;

    /* Store measured data as initial. Will be used as reference for other
     * states
     */
    memcpy(&ms_sensors_cntrl_var.s_mag_init_data, ps_data,
           sizeof(ms_sensors_cntrl_var.s_mag_init_data));

    LOGI(tag, "Magnet detected");

  } else {
    // Magnet is still not present. Not much to do. Stay in current state
  }

  return e_state;
}

static te_states _state_magnet_approaching(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    const tp_event_cb pf_event_cb) {
  // Counts how many times is angle within "zero position" range
  static uint8_t u8_angle_not_change_counter;

  te_states e_state = STATE_MAGNET_APPROACHING;

  /* Check amplitude. If below threshold, magnet is probably getting far from
   * sensor, so state should be changed back to the "not present"
   */
  if (ps_data->i32_amplitude_ut <
      ms_sensors_cntrl_var.s_cfg.i32_mag_detected_threshold_ut) {
    e_state = STATE_MAGNET_NOT_PRESENT;

    // Restart counter so next time start counting from zero
    u8_angle_not_change_counter = 0;

    LOGI(tag, "Magnet removed (approaching)");
  } else {
    /* Calculate angle difference. Angle should not be changed too much
     * before reaching "zero position". Check that too.
     */
    int32_t i32_angle_diff = 0;
    int32_t i32_abs_angle_diff = 0;

    _get_angle_diff(ps_data, &i32_angle_diff, &i32_abs_angle_diff);

    if (i32_abs_angle_diff > ms_sensors_cntrl_var.s_cfg.i32_mag_deadzone_deg) {
      // Magnet is approaching, but bit rotate
      u8_angle_not_change_counter = 0;

      /* Store new "initial" position in order to be able dynamically react for
       * this rotation
       */
      memcpy(&ms_sensors_cntrl_var.s_mag_init_data, ps_data,
             sizeof(ms_sensors_cntrl_var.s_mag_init_data));

    } else {
      // Magnet is approaching and not rotating
      u8_angle_not_change_counter++;
    }

    // If angle value is "stable" over time, change state
    if (u8_angle_not_change_counter >=
        ms_sensors_cntrl_var.s_cfg.u8_mag_approach_stable_angle_cnt) {
      e_state = STATE_MAGNET_ZERO_POSITION;
      u8_angle_not_change_counter = 0;

      LOGD(tag, "Switch to zero position");
      pf_event_cb(EVENT_TYPE_POWER_ON);
    }
  }

  return e_state;
}

static te_states _state_magnet_zero_position(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    const tp_event_cb pf_event_cb) {
  te_states e_state = STATE_MAGNET_ZERO_POSITION;

  // Check if magnet is still there - highest priority
  if (ps_data->i32_amplitude_ut >
      ms_sensors_cntrl_var.s_cfg.i32_mag_detected_threshold_ut) {
    // Magnet still detected -> figure out angle
    int32_t i32_angle_diff = 0;

    // Get absolute value of angle in order to be able determine if still
    // within dead zone or not
    int32_t i32_abs_angle_diff = 0;

    _get_angle_diff(ps_data, &i32_angle_diff, &i32_abs_angle_diff);

    if (i32_abs_angle_diff < ms_sensors_cntrl_var.s_cfg.i32_mag_deadzone_deg) {
      /* Angle rotation is negligible -> update initial vector, since
       * magnetometer have own static offset in time
       */
      memcpy(&ms_sensors_cntrl_var.s_mag_init_data, ps_data,
             sizeof(ms_sensors_cntrl_var.s_mag_init_data));
    } else {
      // Angle change is big enough to do some action
      e_state = STATE_MAGNET_ACTION_POSITION;
    }

  } else {
    // Magnet removed -> change state
    e_state = STATE_MAGNET_NOT_PRESENT;

    LOGI(tag, "Magnet removed (zero position)");

    pf_event_cb(EVENT_TYPE_POWER_OFF);
  }

  return e_state;
}

static te_states _state_magnet_action_position(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    const tp_event_cb pf_event_cb) {
  te_states e_state = STATE_MAGNET_ACTION_POSITION;

  // Check if magnet is still there - highest priority
  if (ps_data->i32_amplitude_ut >
      ms_sensors_cntrl_var.s_cfg.i32_mag_detected_threshold_ut) {
    // Magnet still detected -> figure out angle
    int32_t i32_angle_diff = 0;

    // Get absolute value of angle in order to be able determine if still
    // within dead zone or not
    int32_t i32_abs_angle_diff = 0;

    _get_angle_diff(ps_data, &i32_angle_diff, &i32_abs_angle_diff);

    if (i32_abs_angle_diff <
        ms_sensors_cntrl_var.s_cfg.i32_mag_zero_pos_tol_deg) {
      // Assume that magnet is back at "zero position" -> change state
      e_state = STATE_MAGNET_ZERO_POSITION;
    } else {
      // Magnet is still in "action position"
      if (i32_angle_diff > 0) {
        // Counter clock wise rotation

        pf_event_cb(EVENT_TYPE_DEC_BRIGHTNESS);
      } else {
        // Clock wise rotation
        pf_event_cb(EVENT_TYPE_INC_BRIGHTNESS);
      }
    }
  } else {
    // Magnet removed -> change state
    e_state = STATE_MAGNET_NOT_PRESENT;

    LOGI(tag, "Magnet removed (action position)");

    pf_event_cb(EVENT_TYPE_POWER_OFF);
  }

  return e_state;
}

static e_sensor_error _get_mag_angle_amplitude_timestamp(
    ts_mag_angle_amplitude_timestamp *ps_angle_amplitude_timestamp) {
  // Data from sensor
  ts_sensor_utils_data s_measured_data = {0};

  e_sensor_error e_err_code =
      _get_sensor_data(SENSOR_FEATURE_MAGENETOMETER, &s_measured_data);

  // If reading from sensor failed, there is no meaning to calculate angle and
  // obtain timestamp
  if (e_err_code) {
    return e_err_code;
  }

  // Get timestamp
  ps_angle_amplitude_timestamp->u32_timestamp_ms = sensor_get_time_ms();

  // Expect that magnet is aligned against plane XY
  ps_angle_amplitude_timestamp->i32_angle_deg =
      mf_get_angle_from_xy((int32_t)s_measured_data.u_d.s_vect.i16_x,
                           (int32_t)s_measured_data.u_d.s_vect.i16_y);

  // Get amplitude
  ts_mv_vector_16 s_math_vect = {
      .i16_x = s_measured_data.u_d.s_vect.i16_x,
      .i16_y = s_measured_data.u_d.s_vect.i16_y,
      .i16_z = s_measured_data.u_d.s_vect.i16_z,
  };

  mv_vect_16_length(&s_math_vect,
                    &ps_angle_amplitude_timestamp->i32_amplitude_ut);

  LOGV(tag, "Vect len: %d", ps_angle_amplitude_timestamp->i32_amplitude_ut);

  // Store last measured amplitude
  ms_sensors_cntrl_var.i32_last_measured_mag_amplitude =
      ps_angle_amplitude_timestamp->i32_amplitude_ut;

  return e_err_code;
}

static void _load_settings_from_nvs(ts_nvs_cfg *ps_var) {
  /* Load settings from NVS. Default settings is already at shared variable.
   * So just make copy, so default will be somewhere preserved.
   */
  ts_nvs_cfg s_default;
  memcpy(&s_default, ps_var, sizeof(ts_nvs_cfg));

  te_nvs_sttngs_err e_err_code =
      nvs_settings_get(tag, ps_var, &s_default, sizeof(ts_nvs_cfg));
  if (e_err_code) {
    LOGW(tag, "Load from NVS failed: %s\n   Using default settings",
         nvs_settings_get_err_str(e_err_code));
  }
}

static void _save_settings_to_nvs(ts_nvs_cfg *ps_var) {
  te_nvs_sttngs_err e_err_code =
      nvs_settings_set(tag, ps_var, sizeof(ts_nvs_cfg));
  // If save failed, nothing much to do
  if (e_err_code) {
    LOGE(tag, "Saving to NVS failed: %s", nvs_settings_get_err_str(e_err_code));
  }
}

// =====================| Internal functions: low level |=====================
static void _get_angle_diff(
    const ts_mag_angle_amplitude_timestamp *const ps_data,
    int32_t *pi32_angle_diff, int32_t *pi32_angle_diff_abs) {
  assert(ps_data);
  assert(pi32_angle_diff);
  assert(pi32_angle_diff_abs);

  *pi32_angle_diff = mf_relative_angle_difference(
      ms_sensors_cntrl_var.s_mag_init_data.i32_angle_deg,
      ps_data->i32_angle_deg);

  LOGV(tag, "Angle diff: %d", *pi32_angle_diff);

  // Get absolute value of angle in order to be able determine if still
  // within dead zone or not
  if (*pi32_angle_diff < 0) {
    *pi32_angle_diff_abs = -1 * *pi32_angle_diff;
  } else {
    *pi32_angle_diff_abs = *pi32_angle_diff;
  }
}

static e_sensor_error _get_sensor_data(const te_sensor_feature e_feature,
                                       ts_sensor_utils_data *ps_data) {
  assert((e_feature == SENSOR_FEATURE_MAGENETOMETER) ||
         (e_feature == SENSOR_FEATURE_TEMPERATURE));
  assert(ps_data);

  // Deal with calling this function from multiple threads
  xSemaphoreTake(ms_sensors_cntrl_var.s_mutex, (portTickType)portMAX_DELAY);

  // Count number of retries
  static uint8_t u8_num_of_retry = 0;

  e_sensor_error e_ret_err_code = SENSOR_FAIL;

  e_sensor_error e_sensor_err_code = sensor_get_int(e_feature, ps_data);

  // Check if there was any problem
  if (e_sensor_err_code) {
    LOGE(tag, "Getting data from %s failed: %s",
         sensor_feature_to_str(e_feature),
         sensor_error_code_to_str(e_sensor_err_code));

    u8_num_of_retry++;
    if (u8_num_of_retry > SENSORS_CNTRLR_RETRY_IF_FAIL) {
      LOGE(tag,
           "Getting data from %s failed %d times"
           " in row. Giving up",
           sensor_feature_to_str(e_feature), SENSORS_CNTRLR_RETRY_IF_FAIL);
      // Set return error code
      e_ret_err_code = SENSOR_UNEXPECTED_EXTERNAL_CONDITIONS;

    } else {
      // Error not repeated multiple times yet
      e_ret_err_code = SENSOR_OK;
    }
  } else {
    // No problem. Process data
    // Reset number of retry counter, since now it is working again
    u8_num_of_retry = 0;

    // Print measured data
    LOGV(tag, "%s", sensor_data_to_str(ps_data));

    e_ret_err_code = e_sensor_err_code;
  }

  // Function can be called again
  xSemaphoreGive(ms_sensors_cntrl_var.s_mutex);

  return e_ret_err_code;
}
