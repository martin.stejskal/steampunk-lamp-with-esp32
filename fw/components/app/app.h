/**
 * @file
 * @author Martin Stejskal
 * @brief Application layer for steampunk lamp
 */
#ifndef __APP_H__
#define __APP_H__
// ===============================| Includes |================================
#include "event_handler.h"
#include "overheating_handler.h"
#include "sensors_controller.h"
// ================================| Defines |================================
/**
 * @brief When device change state from OFF to ON, ignore new events for some
 *        time
 *
 * This ignore period have it's purpose. Let's say that device will be enabled
 * by some event, that might repeat in "short time". By repeating inappropriate
 * event it could abort initial "fade in" effect. Therefore there is small
 * window, when all new events are simply ignored.
 */
#define APP_OFF_TO_ON_IGNORE_EVENTS_MS (500)

/**
 * @brief Duration of "blink" effect in milliseconds
 *
 * Blink effect is used for example when light intensity can not go higher.
 */
#define APP_BLINK_DURATION_MS (700)

/**
 * @brief Tells how many times should be "blink" effect repeated
 */
#define APP_BLINK_REPEAT (0)

/**
 * @brief Duration of fade off effect when powering off device in milliseconds
 */
#define APP_POWER_OFF_DURATION_MS (2500)

/**
 * @brief When intensity is 0, switch to power off after this timeout
 *
 * Power off state differs from state when intensity is set to 0. This timeout
 * define after which time should system go to power off state when intensity
 * is unchanged and still 0.
 */
#define APP_AUTO_POWER_OFF_SEC (10 * 60)

/**
 * @brief Sampling period in milliseconds for side jobs
 *
 * Application layer needs to care about automatic power off, re-enable saving
 * state of power LED into NVS, check for overheating events
 *
 * @note This period should be lower than period which is used to report
 *       overheating.
 */
#define APP_SIDE_TASK_PERIOD_MS (SENSORS_CNTRLR_TEMP_PERIOD_MS / 2)

/**
 * @brief Default overheating time and intensity parameters
 *
 * Overheating handler module does not care about temperature. It only cares if
 * overheating is still there or not. Therefore there is not defined
 * temperature threshold, since it is job of the sensors controller.
 *
 * @{
 */
// There are 0~5 stages. Stage 0 means "normal operation", while 1~5 is telling
// duration and severity of overheating. For every stage shall be defined
// maximum PWM value. For stage 1~4, there is necessary also define time
// duration before jump to more aggressive mode (reduce more maximum PWM).
// Values are defined as array

// PWM. Stage 0 ~ stage 5
#define APP_OVERHEATING_PWM \
  { 255, 230, 180, 130, 70, 10 }

// Time. Stage 1 ~ stage 4 is need to be time limited. Stage 0 & 5 are dummy
#define APP_OVERHEATING_SEC \
  { 0, (60 * 2), (60 * 5), (60 * 4), (60 * 3), 0 }

/**
 * @}
 */

/**
 * @brief Define PWM "step" at different intensity values
 *
 * There have to be defined this "last option" too, due to simplifying
 * algorithm which works with this table in order to cover "the last box".
 */
#define APP_INTENSITY_AND_PWM_STEP                                          \
  {                                                                         \
    {.u8_threshold = 30, .u8_step = 1}, {.u8_threshold = 50, .u8_step = 2}, \
        {.u8_threshold = 80, .u8_step = 4},                                 \
        {.u8_threshold = 120, .u8_step = 6},                                \
        {.u8_threshold = 150, .u8_step = 10}, {                             \
      .u8_threshold = 255, .u8_step = 10                                    \
    }                                                                       \
  }

/**
 * @brief Define number of items at APP_INTENSITY_AND_PWM_STEP macro
 *
 * Could not find any more elegant and automatic way. So here it is defined
 * this way
 */
#define APP_INTENSITY_AND_PWM_STEP_ITEMS (6)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
/**
 * @brief Keep together PWM step for every PWM threshold value
 */
typedef struct {
  uint8_t u8_threshold;
  uint8_t u8_step;
} ts_step_threshold_item;

typedef struct {
  // Configuration structure for overheating handler module
  ts_overheating_hander_cfg s_overheating_hander_cfg;

  /**
   * @brief Define PWM step for different current PWM value
   *
   * When current PWM value is low, human eye can recognize difference between
   * PWM=0 and PWM=1. However, when PWM value is for example 150, human eye can
   * barely recognize it from 151. Therefore for different PWM levels are
   * different steps in order to be more user friendly. Table below define
   * thresholds for different PWM steps. It is empiric table. It depends on used
   * HW as well as subjective feel.
   */
  ts_step_threshold_item s_pwm_step[APP_INTENSITY_AND_PWM_STEP_ITEMS];
} ts_app_cfg;
// ===========================| Global variables |============================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Application task
 * @param pv_args Pointer to parameters. None are processed. Dummy
 *        variable
 */
void app_task_rtos(void* pv_args);
// ========================| Middle level functions |=========================
/**
 * @brief Returns pointer to application configuration structure
 *
 * This way is possible to change application configuration "on the fly".
 * However it disables any parameter check. So think twice before change it.
 *
 * @return Pointer to application configuration structure
 */
ts_app_cfg* app_get_configuration(void);

/**
 * @brief Save current settings into NVS
 *
 * Make current settings permanent
 */
void app_save_configuration_to_nvs(void);

// ==========================| Low level functions |==========================
/**
 * @brief Callback from lower layers which drive application state machine
 *
 * Via this function is possible to control application logic from lower
 * layers.
 *
 * @param e_event One of the event type
 */
void app_event_callback(const te_event_type e_event);

#endif  // __APP_H__
