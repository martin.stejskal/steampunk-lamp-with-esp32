/**
 * @file
 * @author Martin Stejskal
 * @brief Application layer for steampunk lamp
 */
// ===============================| Includes |================================
#include "app.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

#include "cfg.h"
#include "ir_controller.h"
#include "log_system.h"
#include "nvs_settings.h"
#include "power_led.h"

// ================================| Defines |================================
#define _NUM_OF_ITEMS(array) (sizeof(array) / sizeof(array[0]))
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

typedef enum {
  APP_STATE_INIT,
  APP_STATE_LAMP,
  APP_STATE_TIMER,
  APP_STATE_OFF,
} te_app_state;

typedef struct {
  // Keep state for state machine safe and sound
  te_app_state e_state;

  // Allow to ignore events for some time if needed
  uint32_t u32_ignore_events_up_to_ms;

  // Last activity timestamp
  uint32_t u32_last_activity_timestamp_ms;

  // Settings stored in NVS
  ts_app_cfg s_nvs_cfg;
} ts_runtime;
// ===========================| Global variables |============================
static char *tag = "App";

static ts_runtime ms_app_var = {
    .e_state = APP_STATE_INIT,
};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void _run_init_fade_task(void);
// ==============| Internal function prototypes: middle level |===============
static void _event_inc_brightness(void);
static void _event_dec_brightness(void);
static void _event_timer(void);
static void _event_power_toggle(void);
static void _event_power_on(void);
static void _event_power_off(void);
// ================| Internal function prototypes: low level |================
/**
 * @brief Called when another task is finished in order to register such event
 *
 */
static void _app_power_led_callback(void);

static void _load_settings_from_nvs(void);
static void _load_default_settings(ts_app_cfg *const ps_nvs_cfg);

static void _activate_timer_event(void);
static void _run_fade_effect(const ts_pwr_led_arg s_fade_arg);
static void _blink_pwr_led(void);

static uint8_t _get_pwm_step(uint8_t u8_pwm_current);

static void _print_state_name(void);
// =========================| High level functions |==========================
void app_task_rtos(void *pv_args) {
  ms_app_var.e_state = APP_STATE_INIT;
  ms_app_var.u32_ignore_events_up_to_ms = 0;

  // Initially, maximum PWM value is 100% (8 bit value)
  uint8_t u8_previous_max_pwm = 0xFF;

  pwr_led_init();

  _load_settings_from_nvs();

  overheating_handler_init(&ms_app_var.s_nvs_cfg.s_overheating_hander_cfg);

  // Deals with event handlers
  xTaskCreate(event_handler_task_rtos, "EventH", 4 * 1024, app_event_callback,
              EVENT_HANDLER_TASK_PRIORITY, NULL);

  // Disable storing LED PWM value for now
  pwr_led_set_store_pwm_to_nvs(false);

  // Store power LED PWM to NVS time to time
  xTaskCreate(pwr_led_store_pwm_value_in_nvs, "PwrLedNvs", 2 * 1024, NULL,
              PWR_LED_STORE_IN_NVS_PRIORITY, NULL);

  _run_init_fade_task();

  while (1) {
    DELAY_MS(APP_SIDE_TASK_PERIOD_MS);

    uint32_t u32_inactivity_time_ms =
        GET_CLK_MS_32BIT() - ms_app_var.u32_last_activity_timestamp_ms;

    /* Auto power off makes sense only when at "Lamp" state. In case of other
     * states, do not mess with events simply.
     */
    if (ms_app_var.e_state == APP_STATE_LAMP) {
      // Check inactivity time before do further processing
      if (u32_inactivity_time_ms > (1000 * APP_AUTO_POWER_OFF_SEC)) {
        uint8_t u8_pwm = pwr_led_get_pwm();

        // Lamp does not light for some time
        if (0 == u8_pwm) {
          app_event_callback(EVENT_TYPE_POWER_OFF);
        }
      }
    }

    // Enable storing LED PWM into NVS if meet conditions
    if (ms_app_var.e_state == APP_STATE_LAMP) {
      // Inactivity on both sensors for some time
      if ((u32_inactivity_time_ms > (10 * IR_TX_PERIOD_MS)) &&
          (u32_inactivity_time_ms > (10 * SENSORS_CNTRLR_MAG_PERIOD_MS))) {
        // If not enabled, enable storing into NVS
        if (!pwr_led_get_store_pwm_to_nvs()) {
          pwr_led_set_store_pwm_to_nvs(true);
        }
      }
    } else {
      /* When comes to disabling storing into NVS, it is done while
       * processing events. It is necessary to disable storing into NVS
       * "immediately" in order to avoid storing some unwanted values.
       */
    }

    // Deal with overheating
    uint8_t u8_new_pwm_max = overheating_handler_task();
    if (u8_new_pwm_max != u8_previous_max_pwm) {
      pwr_led_set_pwm_maximum(u8_new_pwm_max);

      // Make sense to update only if they do not match
      u8_previous_max_pwm = u8_new_pwm_max;

      LOGI(tag, "Changing max PWM due to overheating: %d", u8_new_pwm_max);

      /* Note: in case that overheating passed, it does not make practical
       * sense to restore PWM set by user before overheating, since it would
       * most probably cause overheating again.
       */
    }
  }

  vTaskDelete(0);
}

// ========================| Middle level functions |=========================
ts_app_cfg *app_get_configuration(void) { return &ms_app_var.s_nvs_cfg; }

void app_save_configuration_to_nvs(void) {
  te_nvs_sttngs_err e_err_code = nvs_settings_set(tag, &ms_app_var.s_nvs_cfg,
                                                  sizeof(ms_app_var.s_nvs_cfg));

  if (e_err_code) {
    LOGE(tag, "Saving settings failed: %s",
         nvs_settings_get_err_str(e_err_code));
  }
}

// ==========================| Low level functions |==========================
void app_event_callback(const te_event_type e_event) {
  LOGI(tag, "Rx event: %s", event_get_name(e_event));
  _print_state_name();

  uint32_t u32_current_time = GET_CLK_MS_32BIT();

  ms_app_var.u32_last_activity_timestamp_ms = u32_current_time;

  if (u32_current_time < ms_app_var.u32_ignore_events_up_to_ms) {
    LOGI(tag, "Event ignored");
    return;
  }

  switch (e_event) {
    case EVENT_TYPE_NONE:
      // This is not expected, but we can move on
      LOGW(tag, "Unexpected event \"none\"");
      break;

    case EVENT_TYPE_POWER_TOGGLE:
      _event_power_toggle();
      break;

    case EVENT_TYPE_POWER_ON:
      _event_power_on();
      break;

    case EVENT_TYPE_POWER_OFF:
      _event_power_off();
      break;

    case EVENT_TYPE_INC_BRIGHTNESS:
      _event_inc_brightness();

      break;
    case EVENT_TYPE_DEC_BRIGHTNESS:
      _event_dec_brightness();
      break;

    case EVENT_TYPE_TIMER:
      _event_timer();
      break;

    case EVENT_TYPE_OVERHEATING:
      // Register overheating event
      overheating_handler_overheating_event(u32_current_time);
      LOGW(tag, "Module is overheating!!!");
      break;
    case EVENT_TYPE_MAX:
      LOGW(tag, "Unexpected event");
      break;
  }

  _print_state_name();
}

// ====================| Internal functions: high level |=====================
static void _run_init_fade_task(void) {
  // Run initial fade effect
  ts_pwr_led_arg s_fade_arg = {.u32_duration_ms = 0,
                               .u16_step_ms = 60,
                               .p_finished_cb = _app_power_led_callback,
                               .p_no_change_cb = _app_power_led_callback};

  s_fade_arg.u8_target_pwm = pwr_led_get_pwm_from_nvs();

  _run_fade_effect(s_fade_arg);
}
// ===================| Internal functions: middle level |====================
static void _event_inc_brightness(void) {
  if (ms_app_var.e_state == APP_STATE_TIMER) {
    // Do nothing. In timer mode this event have not meaning
  } else if (ms_app_var.e_state == APP_STATE_OFF) {
    // This is basically "power on" event too
    _event_power_on();

  } else {
    uint8_t u8_pwm_step = _get_pwm_step(pwr_led_get_pwm());
    pwr_led_inc_bright(u8_pwm_step);
  }
}

static void _event_dec_brightness(void) {
  if ((ms_app_var.e_state == APP_STATE_TIMER) ||
      (ms_app_var.e_state == APP_STATE_OFF)) {
    // Do nothing. In states above this event have no meaning
  } else {
    uint8_t u8_pwm_step = _get_pwm_step(pwr_led_get_pwm());
    pwr_led_dec_bright(u8_pwm_step);
  }
}

static void _event_timer(void) {
  switch (ms_app_var.e_state) {
    case APP_STATE_INIT:
      _activate_timer_event();
      break;

    case APP_STATE_LAMP:
      // Most common use case. Change lamp mode to timer mode
      _activate_timer_event();
      break;
    case APP_STATE_TIMER:
      // Received another timer event. Deactivation blink cause change PWM,
      // which actually terminate current fade task
      _blink_pwr_led();

      ms_app_var.e_state = APP_STATE_LAMP;
      break;
    case APP_STATE_OFF:
      // Do nothing. Timer event have no power in here!
      break;
  }
}

static void _event_power_toggle(void) {
  if (ms_app_var.e_state == APP_STATE_OFF) {
    _event_power_on();
  } else {
    _event_power_off();
  }
}

static void _event_power_on(void) {
  // Ignore new events for some time in order to avoid accident aborting
  // initial fade in.
  ms_app_var.u32_ignore_events_up_to_ms =
      GET_CLK_MS_32BIT() + APP_OFF_TO_ON_IGNORE_EVENTS_MS;

  // No longer off
  ms_app_var.e_state = APP_STATE_INIT;
  _run_init_fade_task();
}

static void _event_power_off(void) {
  // If power off is called any any other state, it mean power off now

  // Disable storing values to NVS. After another power on we will like to
  // recover to original value
  pwr_led_set_store_pwm_to_nvs(false);

  ms_app_var.u32_ignore_events_up_to_ms =
      GET_CLK_MS_32BIT() + APP_POWER_OFF_DURATION_MS;

  ts_pwr_led_arg s_fade_arg = {.u8_target_pwm = 0,
                               .u32_duration_ms = APP_POWER_OFF_DURATION_MS,
                               .u16_step_ms = 0,
                               .p_finished_cb = 0,
                               .p_no_change_cb = 0};

  ms_app_var.e_state = APP_STATE_OFF;

  _run_fade_effect(s_fade_arg);
}
// =====================| Internal functions: low level |=====================
static void _app_power_led_callback(void) {
  _print_state_name();

  switch (ms_app_var.e_state) {
    case APP_STATE_INIT:
      // Initial fade in done
      ms_app_var.e_state = APP_STATE_LAMP;
      break;
    case APP_STATE_LAMP:
      // In regular lamp mode is not expected callback from LED fade task
      assert(0);
      break;
    case APP_STATE_TIMER:
      // Timer task finished -> go to off mode. Just change state. Nothing
      // more is needed. Power LED is already off
      ms_app_var.e_state = APP_STATE_OFF;
      break;
    case APP_STATE_OFF:
      // In off state, we do not expect any callback from power LED module
      assert(0);
      break;
  }

  _print_state_name();
}

static void _load_settings_from_nvs(void) {
  ts_app_cfg s_default;

  _load_default_settings(&s_default);

  te_nvs_sttngs_err e_err_code = nvs_settings_get(
      tag, &ms_app_var.s_nvs_cfg, &s_default, sizeof(ms_app_var.s_nvs_cfg));

  if (e_err_code) {
    LOGW(tag, "Load from NVS failed: %s\n   Using default settings",
         nvs_settings_get_err_str(e_err_code));
  }
}

static void _load_default_settings(ts_app_cfg *const ps_nvs_cfg) {
  const ts_app_cfg s_default = {
      .s_overheating_hander_cfg =
          {
              .au16_time_profile_sec = APP_OVERHEATING_SEC,
              .au8_pwm_profile = APP_OVERHEATING_PWM,

              // This is practically constant. Should not be changed here
              .u32_temperature_sensor_period_ms = SENSORS_CNTRLR_TEMP_PERIOD_MS,
          },
      .s_pwm_step = APP_INTENSITY_AND_PWM_STEP,
  };

  memcpy(ps_nvs_cfg, &s_default, sizeof(s_default));
}

static void _activate_timer_event(void) {
  // Fade off parameters
  ts_pwr_led_arg s_fade_arg = {.u8_target_pwm = 0,
                               .u32_duration_ms = 0,
                               .u16_step_ms = 1500,
                               .p_finished_cb = _app_power_led_callback,
                               .p_no_change_cb = _app_power_led_callback};
  // Disable storing values to NVS. After another power on we will like to
  // recover to original value
  pwr_led_set_store_pwm_to_nvs(false);

  // Blink to signalize user, that command is processed
  _blink_pwr_led();

  // Initial fade effect is running. Just run timer over it
  _run_fade_effect(s_fade_arg);

  ms_app_var.e_state = APP_STATE_TIMER;
}

static void _run_fade_effect(const ts_pwr_led_arg s_fade_arg) {
  // Use static, so variables will be not rewritten after quitting this
  // function
  static ts_pwr_led_arg s_fade_arg_copy;
  memcpy(&s_fade_arg_copy, &s_fade_arg, sizeof(s_fade_arg_copy));

  xTaskCreate(pwr_led_run_fade_task, "LedFade", 2 * 1024, &s_fade_arg_copy,
              PWR_LED_FADE_EFFECT_PRIORITY, NULL);
}

static void _blink_pwr_led(void) {
  // Plan is to get current PWM, calculate suitable PWM values to be able
  // actually watch blink and perform blink itself
  uint8_t u8_current_pwm = pwr_led_get_pwm();

  uint8_t u8_a_pwm;
  uint8_t u8_b_pwm;

  // If intensity is quite high, MSb is set
  if (u8_current_pwm > 127) {
    // First phase reduce light intensity
    u8_a_pwm = u8_current_pwm >> 1;
    u8_b_pwm = u8_current_pwm;
  } else {
    // Intensity is lower. Increase intensity
    u8_a_pwm = 0x80 | u8_current_pwm;
    u8_b_pwm = u8_current_pwm;
  }

  pwr_led_blink(u8_a_pwm, u8_b_pwm, APP_BLINK_DURATION_MS,
                APP_BLINK_DURATION_MS, APP_BLINK_REPEAT);
}

static uint8_t _get_pwm_step(uint8_t u8_pwm_current) {
  uint8_t u8_step = 0;

  // Go through table until current value is under threshold
  for (uint8_t u8_idx = 0;
       u8_idx < _NUM_OF_ITEMS(ms_app_var.s_nvs_cfg.s_pwm_step); u8_idx++) {
    if (u8_pwm_current <=
        ms_app_var.s_nvs_cfg.s_pwm_step[u8_idx].u8_threshold) {
      u8_step = ms_app_var.s_nvs_cfg.s_pwm_step[u8_idx].u8_step;
      break;
    }
  }

  // Should be changed to non-zero value. Always.
  assert(u8_step);

  return u8_step;
}

static void _print_state_name(void) {
  const char *pac_str = "???";

  switch (ms_app_var.e_state) {
    case APP_STATE_INIT:
      pac_str = "Init";
      break;
    case APP_STATE_LAMP:
      pac_str = "Lamp";
      break;
    case APP_STATE_TIMER:
      pac_str = "Timer";
      break;
    case APP_STATE_OFF:
      pac_str = "Off";
      break;
  }

  LOGD(tag, "State: %s", pac_str);
}
