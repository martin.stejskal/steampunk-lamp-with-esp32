/**
 * @file
 * @author Martin Stejskal
 * @brief Common place for event related stuff
 */
// ===============================| Includes |================================
#include "events.h"
// ================================| Defines |================================
#ifndef NULL
#define NULL (0)
#endif
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
const char *event_get_name(const te_event_type e_event) {
  switch (e_event) {
    case EVENT_TYPE_POWER_TOGGLE:
      return "Power toggle";
    case EVENT_TYPE_POWER_ON:
      return "Power on";
    case EVENT_TYPE_POWER_OFF:
      return "Power off";
    case EVENT_TYPE_INC_BRIGHTNESS:
      return "Increase brightness";
    case EVENT_TYPE_DEC_BRIGHTNESS:
      return "Decrease brightness";
    case EVENT_TYPE_TIMER:
      return "Timer";
    case EVENT_TYPE_OVERHEATING:
      return "Overheating";
    case EVENT_TYPE_NONE:
      return NULL;
    case EVENT_TYPE_MAX:
      return NULL;
  }

  // This case should not happen, but if so, return "void" pointer
  return NULL;
}
// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
