/**
 * @file
 * @author Martin Stejskal
 * @brief Common place for event related stuff
 */
#ifndef __EVENTS_H__
#define __EVENTS_H__
// ===============================| Includes |================================

// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
/**
 * @brief List of events used across project
 */
typedef enum {
  EVENT_TYPE_NONE,       //!< Dummy event
  EVENT_TYPE_POWER_TOGGLE,      //!< Power toggle
  EVENT_TYPE_POWER_ON,   //!< Power on
  EVENT_TYPE_POWER_OFF,  //!< Power off

  EVENT_TYPE_INC_BRIGHTNESS,  //!< Increase brightness
  EVENT_TYPE_DEC_BRIGHTNESS,  //!< Decrease brightness

  EVENT_TYPE_TIMER,  //!< Timer function

  EVENT_TYPE_OVERHEATING,  //!< Alarm application layer about high temperature

  EVENT_TYPE_MAX  //!< Last item in list. No meaning
} te_event_type;

/**
 * @brief Define pointer to event callback
 *
 * This pointer type is expected to be passed to @ref eventHandlerTaskRTOS
 */
typedef void (*tp_event_cb)(const te_event_type);
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Get event name as string
 * @param e_event Event type
 * @return Pointer to string
 */
const char* event_get_name(const te_event_type e_event);

#endif  // __EVENTS_H__
