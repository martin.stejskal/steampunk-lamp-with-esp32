/**
 * @file
 * @author Martin Stejskal
 * @brief User/project specific module for UI service code
 */
// ===============================| Includes |================================

#include "ui_service_user.h"

#include <driver/gpio.h>
#include <driver/uart.h>
#include <esp_system.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

#include "app.h"
#include "cfg.h"
#include "ir_controller.h"
#include "log_system.h"
#include "nvs_settings.h"
#include "power_led.h"
#include "sensors_controller.h"
#include "ui_service_core.h"
#include "ui_service_user.h"
#include "ui_service_utils.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char* tag = "UI service (u)";

static const ts_usc_cmd_cfg mas_user_cmds[] = {
    {
        .pac_command = "get pwm",
        .pac_help = "Get current PWM value for power LED",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_pwm,
    },
    {
        .pac_command = "set pwm",
        .pac_help =
            "Try to set PWM value for power LED. This still honors overheating "
            "profiles. So if device is overheating, you can not set full power "
            "anyways. This is due o safety reason",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_pwm,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = 0,
                .i32_max = UINT8_MAX,
                .pac_argument_example = "23",
            },
    },
    {
        .pac_command = "get ir addr",
        .pac_help = "Get current IR address for device",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_ir_addr,
    },
    {
        .pac_command = "set ir addr",
        .pac_help = "Set new IR address for device. 16 bit hex value",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_ir_addr,

        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_HEX,
                .i32_min = 0,
                .i32_max = UINT16_MAX,
                .pac_argument_example = "BEEF",
            },
    },
    {
        .pac_command = "get ir cmd list",
        .pac_help = "Prints list of commands and assigned index in list",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_ir_cmd_list,
    },
    {
        .pac_command = "get ir cmd",
        .pac_help = "Print command value for given command index. Refer to the "
                    "\"get ir cmd list\" command to get list of commands "
                    "in human-readable form",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_get_ir_cmd,

        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = (te_ir_cmds)0,
                      .i32_max = (IR_CNTRL_CMD_MAX - 1),
                      .pac_argument_example = "2"},
    },
    {
        .pac_command = "set ir cmd",
        .pac_help = "Set IR command value for specific command index",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_ir_cmd,

        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_STR,
                .pac_argument_example = "1 0x35",
            },
    },
    {
        .pac_command = "get dac inc",
        .pac_help = "Get DAC raw value for \"incrementation\" function",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_dac_inc,
    },
    {
        .pac_command = "get dac dec",
        .pac_help = "Get DAC raw value for \"decrementation\" function",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_dac_dec,
    },
    {
        .pac_command = "set dac inc",
        .pac_help = "Set DAC raw value for \"incrementation\" function."
                    " 8 bit value",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_dac_inc,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = 0,
                      .i32_max = UINT8_MAX,
                      .pac_argument_example = "113"},
    },
    {
        .pac_command = "set dac dec",
        .pac_help = "Set DAC raw value for \"decrementation\" function."
                    " 8 bit value",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_dac_dec,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                      .i32_min = 0,
                      .i32_max = UINT8_MAX,
                      .pac_argument_example = "109"},
    },
    {
        .pac_command = "get prof pwm",
        .pac_help =
            "Get PWM step profile. When changing intensity, it shall not be "
            "done linear. Problem is that human eye is logarithmic. Therefore "
            "for different light intensity is more suitable different step "
            "when increasing or decreasing light intensity. And this "
            "parameters define this step fir different PWM thresholds",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_pwm_step_profile,
    },
    {
        .pac_command = "set prof pwm th",
        .pac_help = "Set PWM thresholds. See \"get pwm profile\" command help",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_pwm_step_thresholds,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_STR,
                      .pac_argument_example = "30,50,80,120,150,255"},
    },
    {
        .pac_command = "set prof pwm step",
        .pac_help = "Set PWM step. See \"get prof pwm\" command help",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_pwm_step_steps,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_STR,
                      .pac_argument_example = "1,2,4,6,10,10"},
    },
    {
        .pac_command = "get temp",
        .pac_help = "Get current temperature in degrees of Celsius",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_temp,
    },
    {
        .pac_command = "get overht",
        .pac_help = "Get all overheating related parameters at once. This "
                    "include current temperature",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_overheat_params,
    },
    {
        .pac_command = "set overht temp",
        .pac_help =
            "Set overheating temperature threshold. Note that temperature "
            "sensors it not attached to power components. It is typically "
            "reading from ambient air on board with other sensors",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_overheat_temp,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = INT8_MIN,
                .i32_max = INT8_MAX,
                .pac_argument_example = "45",
            },
    },
    {
        .pac_command = "set overht pwm",
        .pac_help = "Set overheating profile for PWM. You need to define PWM "
                    "value for every stage. See \"get overht\" command for "
                    "example",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_overheat_pwm,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_STR,
                      .pac_argument_example = "255,230,180,130,70,10"},
    },
    {
        .pac_command = "set overht time",
        .pac_help =
            "Set overheating profile for time. You need to define "
            "timing for stage 1 ~ 4 only. See \"get overht\" command for "
            "example",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_overheat_time,
        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_STR,
                      .pac_argument_example = "120,300,240,180"},
    },
    {
        .pac_command = "get mag thr",
        .pac_help =
            "Get threshold in uT for detecting external magnet close to "
            "the magnetometer",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_mag_thrshld_ut,
    },
    {
        .pac_command = "set mag thr",
        .pac_help = "Set threshold in uT for detecting external magnet close "
                    "to the magnetometer",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_mag_thrshld_ut,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = 0,
                .i32_max = INT32_MAX,
                .pac_argument_example = "300",
            },
    },
    {
        .pac_command = "get mag amp",
        .pac_help = "Get magnetic field amplitude in uT",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_mag_ampliture_ut,
    },
    {
        .pac_command = "get stable angle",
        .pac_help =
            "Get threshold for estimating that magnet angle is stable and "
            "worth to consider that magnet is at \"zero position\". Value is "
            "just counter for number of measurements",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_stable_angle_cnt,
    },
    {
        .pac_command = "set stable angle",
        .pac_help =
            "Set threshold for estimating zero position of magnet. This value "
            "express number of readings from magnetometer within which "
            "estimated magnetic field angle did not changes. In other words: "
            "this helps to verify that magnet is in \"zero position\"",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_stable_angle_cnt,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = 0,
                .i32_max = UINT8_MAX,
                .pac_argument_example = "10",
            },
    },
    {
        .pac_command = "get mag deadzone",
        .pac_help =
            "Get deadzone angle in degrees. Deadzone angle helps with keeping "
            "logic in \"zero position\" state when magnet is rotating bit or "
            "when magnetometer gives incorrect angle due to drifting",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_mag_deadzone_deg,
    },
    {
        .pac_command = "set mag deadzone",
        .pac_help =
            "Set deadzone angle in degrees. Deadzone angle helps with keeping "
            "logic in \"zero position\" state when magnet is rotating bit or "
            "when magnetometer gives incorrect angle due to drifting",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_mag_deadzone_deg,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = 0,
                .i32_max = UINT16_MAX,
                .pac_argument_example = "30",
            },
    },
    {
        .pac_command = "get mag zero pos tol",
        .pac_help = "Get zero position tolerance in degrees. This tolerance is "
                    "used when rotating magnet back to the zero position",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_get_mag_zero_pos_tol_deg,
    },
    {
        .pac_command = "set mag zero pos tol",
        .pac_help = "Set zero position tolerance in degrees. This tolerance is "
                    "used when rotating magnet back to the zero position",
        .b_require_argument = true,
        .pf_cmd_callback = ui_service_cmd_set_mag_zero_pos_tol_deg,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = 0,
                .i32_max = UINT16_MAX,
                .pac_argument_example = "30",
            },
    },
    {
        .pac_command = "factory reset",
        .pac_help = "Delete all stored settings. Recover factory defaults. "
                    "Device will be rebooted automatically.",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_factory_reset,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = 0,
                .i32_max = UINT16_MAX,
                .pac_argument_example = "15",
            },
    },
    {
        .pac_command = "reboot",
        .pac_help = "Reboot system",
        .b_require_argument = false,
        .pf_cmd_callback = ui_service_cmd_reboot,
    }};

/**
 * @brief Keep track about UART driver state
 */
static bool mb_uart_driver_installed = false;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
te_usc_error_code _read_array_i32(const char* pac_str,
                                  const uint8_t u8_num_of_items,
                                  int32_t* pi32_buffer, const int32_t i32_min,
                                  const int32_t i32_max);
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
const ts_usc_cmd_cfg* ui_service_user_get_list_of_cmds(void) {
  return mas_user_cmds;
}

uint16_t ui_service_user_get_num_of_cmds(void) {
  return (sizeof(mas_user_cmds) / sizeof(mas_user_cmds[0]));
}

// ===============================| Commands |================================
void ui_service_cmd_get_pwm(ts_usc_cb_args* ps_cb_args) {
  uint8_t u8_pwm = pwr_led_get_pwm();

  uint16_t u16_percentage = ((uint16_t)u8_pwm * 100) / UINT8_MAX;

  usc_printf("%d (%d %%)", u8_pwm, u16_percentage);
}

void ui_service_cmd_set_pwm(ts_usc_cb_args* ps_cb_args) {
  // No error code returned -> always OK
  pwr_led_set_pwm(ps_cb_args->u_value.u32_value);

  usc_print_str(usc_txt_ok());
}

void ui_service_cmd_get_ir_addr(ts_usc_cb_args* ps_cb_args) {
  usc_printf("0x%X", ir_controller_get_address());
}

void ui_service_cmd_set_ir_addr(ts_usc_cb_args* ps_cb_args) {
  usc_print_ok_fail(ir_controller_set_address(ps_cb_args->u_value.u32_value));
}

void ui_service_cmd_get_ir_cmd_list(ts_usc_cb_args* ps_cb_args) {
  // Start from first event after "none" (that is why +1)
  for (te_ir_cmds eCmdId = (te_ir_cmds)0; eCmdId < IR_CNTRL_CMD_MAX; eCmdId++) {
    usc_printf("%d : %s\n", eCmdId, ir_controller_get_cmd_name(eCmdId));
  }
}

void ui_service_cmd_get_ir_cmd(ts_usc_cb_args* ps_cb_args) {
  te_ir_cmds e_cmd_id = (te_ir_cmds)ps_cb_args->u_value.u32_value;

  // This range should be checked when processing user input actually
  assert(e_cmd_id < IR_CNTRL_CMD_MAX);

  uint8_t u8_cmd = ir_controller_get_cmd_value(e_cmd_id);

  usc_printf("%s : 0x%02X", ir_controller_get_cmd_name(e_cmd_id), u8_cmd);
}

void ui_service_cmd_set_ir_cmd(ts_usc_cb_args* ps_cb_args) {
  int32_t i32_cmd_idx;
  uint32_t u32_cmd_value;

  // Need to extract 2 values. First is command index, second is value
  esp_err_t e_err_code =
      usu_str_dec_to_int_32(ps_cb_args->pac_args, &i32_cmd_idx);

  if (e_err_code) {
    usc_print_str(usc_txt_can_not_convert_to_int());
    return;
  }

  if ((i32_cmd_idx >= 0) || (i32_cmd_idx < IR_CNTRL_CMD_MAX)) {
    // Range is fine
  } else {
    usc_print_str(usc_txt_out_of_range());
    return;
  }

  // Now get 2nd parameter - command value. Need to find space which separate
  // 2nd argument
  uint16_t u8_event_string_length = usu_get_word_length(ps_cb_args->pac_args);

  const char* pac_cmd_arg = ps_cb_args->pac_args + u8_event_string_length;
  // Check if not NULL -> that would signalize missing 2nd argument
  if ((*pac_cmd_arg) == '\0') {
    usc_print_str("Command argument not found");
    return;
  }

  e_err_code = usu_str_hex_to_uint_32(pac_cmd_arg, &u32_cmd_value);

  if (e_err_code) {
    usc_print_str(usc_txt_can_not_convert_to_int());
    return;
  }

  // Only 8 bit value is supported
  if (u32_cmd_value > 0xFF) {
    usc_print_str(usc_txt_out_of_range());
    return;
  }

  assert(u32_cmd_value <= 0xFF);

  // OK, so here all values should be valid
  e_err_code = ir_controller_set_cmd_value((te_ir_cmds)i32_cmd_idx,
                                           (uint8_t)u32_cmd_value);

  // Inform user about result
  usc_print_ok_fail(e_err_code);
}

void ui_service_cmd_get_dac_inc(ts_usc_cb_args* ps_cb_args) {
  usc_printf("%d", ir_controller_get_dac(IR_CNTRL_FNC_INC));
}

void ui_service_cmd_get_dac_dec(ts_usc_cb_args* ps_cb_args) {
  usc_printf("%d", ir_controller_get_dac(IR_CNTRL_FNC_DEC));
}

void ui_service_cmd_set_dac_inc(ts_usc_cb_args* ps_cb_args) {
  usc_print_ok_fail(
      ir_controller_set_dac(IR_CNTRL_FNC_INC, ps_cb_args->u_value.u32_value));
}

void ui_service_cmd_set_dac_dec(ts_usc_cb_args* ps_cb_args) {
  usc_print_ok_fail(
      ir_controller_set_dac(IR_CNTRL_FNC_DEC, ps_cb_args->u_value.u32_value));
}

void ui_service_cmd_get_pwm_step_profile(ts_usc_cb_args* ps_cb_args) {
  // Load application settings
  const ts_app_cfg* p_cfg = app_get_configuration();

  for (uint8_t u8_idx = 0; u8_idx < APP_INTENSITY_AND_PWM_STEP_ITEMS;
       u8_idx++) {
    usc_printf("PWM threshold: %d    PWM step: %d\n",
               p_cfg->s_pwm_step[u8_idx].u8_threshold,
               p_cfg->s_pwm_step[u8_idx].u8_step);
  }
}

void ui_service_cmd_set_pwm_step_thresholds(ts_usc_cb_args* ps_cb_args) {
  // Parse user input and try to get values into array below
  int32_t ai32_value_buffer[APP_INTENSITY_AND_PWM_STEP_ITEMS];

  te_usc_error_code e_err_code =
      _read_array_i32(ps_cb_args->pac_args, APP_INTENSITY_AND_PWM_STEP_ITEMS,
                      ai32_value_buffer, 0, UINT8_MAX);

  if (USC_OK == e_err_code) {
    // Write settings to application configuration file
    ts_app_cfg* ps_cfg = app_get_configuration();

    for (uint8_t u8_item_idx = 0;
         u8_item_idx < APP_INTENSITY_AND_PWM_STEP_ITEMS; u8_item_idx++) {
      ps_cfg->s_pwm_step[u8_item_idx].u8_threshold =
          (uint8_t)ai32_value_buffer[u8_item_idx];
    }

    // Make settings permanent
    app_save_configuration_to_nvs();
  }

  usc_print_ok_fail(e_err_code);
}
void ui_service_cmd_set_pwm_step_steps(ts_usc_cb_args* ps_cb_args) {
  // Parse user input and try to get values into array below
  int32_t ai32_value_buffer[APP_INTENSITY_AND_PWM_STEP_ITEMS];

  te_usc_error_code e_err_code =
      _read_array_i32(ps_cb_args->pac_args, APP_INTENSITY_AND_PWM_STEP_ITEMS,
                      ai32_value_buffer, 0, UINT8_MAX);

  if (USC_OK == e_err_code) {
    // Write settings to application configuration file
    ts_app_cfg* ps_cfg = app_get_configuration();

    for (uint8_t u8_item_idx = 0;
         u8_item_idx < APP_INTENSITY_AND_PWM_STEP_ITEMS; u8_item_idx++) {
      ps_cfg->s_pwm_step[u8_item_idx].u8_step =
          (uint8_t)ai32_value_buffer[u8_item_idx];
    }

    // Make settings permanent
    app_save_configuration_to_nvs();
  }

  usc_print_ok_fail(e_err_code);
}

void ui_service_cmd_get_temp(ts_usc_cb_args* ps_cb_args) {
  usc_printf("Current temperature: %d deg C\n", sensors_cntrlr_get_temp());
}

void ui_service_cmd_get_overheat_params(ts_usc_cb_args* ps_cb_args) {
  const ts_app_cfg* p_cfg = app_get_configuration();

  usc_printf(
      "Current temperature:  %d deg C\n"
      "Overheat temperature: %d deg C\n"
      "Sensor sample time:   %d ms\n",

      sensors_cntrlr_get_temp(), sensors_cntrlr_get_temp_thrshld(),
      p_cfg->s_overheating_hander_cfg.u32_temperature_sensor_period_ms);

  // Go through every overheating stage and print timeout and PWM intensity
  for (te_overheating_handler_stages e_stage = (te_overheating_handler_stages)0;
       e_stage < OVERHEATING_H_STAGE_MAX; e_stage++) {
    usc_printf("  Stage: %d PWM: %d Time: %d sec\n", e_stage,
               p_cfg->s_overheating_hander_cfg.au8_pwm_profile[e_stage],
               p_cfg->s_overheating_hander_cfg.au16_time_profile_sec[e_stage]);
  }
}

void ui_service_cmd_set_overheat_temp(ts_usc_cb_args* ps_cb_args) {
  sensors_cntrlr_set_temp_thrshld((int8_t)ps_cb_args->u_value.i32_value);

  // Assume that UI core handles boundaries correctly, always OK
  usc_print_ok_fail(USC_OK);
}

void ui_service_cmd_set_overheat_pwm(ts_usc_cb_args* ps_cb_args) {
  // Parse user input and try to get values into array below
  int32_t ai32_value_buffer[OVERHEATING_H_STAGE_MAX];

  te_usc_error_code e_err_code =
      _read_array_i32(ps_cb_args->pac_args, OVERHEATING_H_STAGE_MAX,
                      ai32_value_buffer, 0, UINT8_MAX);

  // If all values are valid, process them
  if (USC_OK == e_err_code) {
    ts_app_cfg* p_cfg = app_get_configuration();

    for (te_overheating_handler_stages e_stage = 0;
         e_stage < OVERHEATING_H_STAGE_MAX; e_stage++) {
      p_cfg->s_overheating_hander_cfg.au8_pwm_profile[e_stage] =
          (uint8_t)ai32_value_buffer[e_stage];
    }

    // Make settings permanent
    app_save_configuration_to_nvs();
  }

  usc_print_str(usc_txt_ok());
}

void ui_service_cmd_set_overheat_time(ts_usc_cb_args* ps_cb_args) {
  // Parse user input and try to get values into array below. Only stage 1~4 is
  // necessary to define
  const uint8_t u8_num_of_items =
      OVERHEATING_H_STAGE_4 - OVERHEATING_H_STAGE_1 + 1;
  int32_t ai32_value_buffer[u8_num_of_items];

  te_usc_error_code e_err_code = _read_array_i32(
      ps_cb_args->pac_args, u8_num_of_items, ai32_value_buffer, 0, UINT16_MAX);

  // If all values are valid, process them
  if (USC_OK == e_err_code) {
    ts_app_cfg* p_cfg = app_get_configuration();

    for (te_overheating_handler_stages e_stage = OVERHEATING_H_STAGE_1;
         e_stage <= OVERHEATING_H_STAGE_4; e_stage++) {
      p_cfg->s_overheating_hander_cfg.au16_time_profile_sec[e_stage] =
          (uint16_t)ai32_value_buffer[e_stage - OVERHEATING_H_STAGE_1];
    }

    // Make settings permanent
    app_save_configuration_to_nvs();
  }

  usc_print_str(usc_txt_ok());
}

void ui_service_cmd_get_mag_thrshld_ut(ts_usc_cb_args* ps_cb_args) {
  usc_printf("%d uT", sensors_cntrlr_get_mag_threshold_ut());
}
void ui_service_cmd_set_mag_thrshld_ut(ts_usc_cb_args* ps_cb_args) {
  sensors_cntrl_set_mag_threshold_ut(ps_cb_args->u_value.i32_value);

  // Assume that UI core handles boundaries correctly, always OK
  usc_print_ok_fail(USC_OK);
}
void ui_service_cmd_get_mag_ampliture_ut(ts_usc_cb_args* ps_cb_args) {
  usc_printf("%d uT", sensors_cntrlr_get_mag_amplitude_ut());
}

void ui_service_cmd_get_stable_angle_cnt(ts_usc_cb_args* ps_cb_args) {
  usc_printf("%dx", sensors_cntrlr_get_stable_angle_cnt());
}
void ui_service_cmd_set_stable_angle_cnt(ts_usc_cb_args* ps_cb_args) {
  sensors_cntrlr_set_stable_angle_cnt(ps_cb_args->u_value.i32_value);

  // Assume that UI core handles boundaries correctly, always OK
  usc_print_ok_fail(USC_OK);
}

void ui_service_cmd_get_mag_deadzone_deg(ts_usc_cb_args* ps_cb_args) {
  usc_printf("%d deg", sensors_cntrlr_get_mag_deadzone_deg());
}
void ui_service_cmd_set_mag_deadzone_deg(ts_usc_cb_args* ps_cb_args) {
  sensors_cntrlr_set_mag_deadzone_deg(ps_cb_args->u_value.i32_value);

  // Assume that UI core handles boundaries correctly, always OK
  usc_print_ok_fail(USC_OK);
}

void ui_service_cmd_get_mag_zero_pos_tol_deg(ts_usc_cb_args* ps_cb_args) {
  usc_printf("%d deg", sensors_cntrlr_get_mag_zero_pos_tol_deg());
}
void ui_service_cmd_set_mag_zero_pos_tol_deg(ts_usc_cb_args* ps_cb_args) {
  sensors_cntrlr_set_mag_zero_pos_tol_deg(ps_cb_args->u_value.i32_value);

  // Assume that UI core handles boundaries correctly, always OK
  usc_print_ok_fail(USC_OK);
}

void ui_service_cmd_factory_reset(ts_usc_cb_args* ps_cb_args) {
  const te_nvs_sttngs_err e_nvs_err = nvs_settings_del_all();

  te_usc_error_code e_err_code;

  if (NVS_STTNGS_OK == e_nvs_err) {
    e_err_code = USC_OK;
  } else {
    e_err_code = USC_ERROR;
  }

  usc_print_ok_fail(e_err_code);

  if (USC_OK == e_err_code) {
    for (uint8_t u8_reboot_countdown =
             UI_SERVICE_USER_FACTORY_RESET_REBOOT_TIMEOUT_SEC;
         u8_reboot_countdown > 0; u8_reboot_countdown--) {
      usc_printf("\nRebooting in %d seconds...", u8_reboot_countdown);
      DELAY_MS(1000);
    }
    ui_service_cmd_reboot(ps_cb_args);
  }
}

void ui_service_cmd_reboot(ts_usc_cb_args* ps_cb_args) { esp_restart(); }

// ===========================| Interface related |===========================
te_usc_error_code usu_uart_init(void) {
  esp_err_t e_err_code;
  // =================================| UART |==============================
  const uart_config_t s_uart_config = {.baud_rate = UI_SERVICE_UART_BAUDRATE,
                                       .data_bits = UART_DATA_8_BITS,
                                       .parity = UART_PARITY_DISABLE,
                                       .stop_bits = UART_STOP_BITS_1,
                                       .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};
  // Try to remove driver if already installed
  if (mb_uart_driver_installed) {
    uart_driver_delete(UI_SERVICE_UART_INTERFACE);
  }

  e_err_code = uart_param_config(UI_SERVICE_UART_INTERFACE, &s_uart_config);
  if (e_err_code) {
    LOGE(tag, "UART configuration failed");
    return ESP_FAIL;
  }

  e_err_code =
      uart_set_pin(UI_SERVICE_UART_INTERFACE, UI_SERVICE_TXD_PIN,
                   UI_SERVICE_RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
  if (e_err_code) {
    LOGE(tag, "UART pin set fail");
    return ESP_FAIL;
  }

  e_err_code = uart_driver_install(UI_SERVICE_UART_INTERFACE,
                                   UI_SERVICE_ITF_BUFFER_SIZE, 0, 0, NULL, 0);
  if (e_err_code) {
    LOGE(tag, "UART driver install failed: %d", e_err_code);
    return ESP_FAIL;
  } else {
    mb_uart_driver_installed = true;
  }
  return ESP_OK;
}

te_usc_error_code usu_uart_read(uint8_t* pu8_data,
                                const uint16_t u16_buffer_length,
                                const uint16_t u16_timeout_ms,
                                uint16_t* pu_num_of_read_bytes) {
  int i_rx_bytes_cnd = uart_read_bytes(UI_SERVICE_UART_INTERFACE, pu8_data,
                                       u16_buffer_length, u16_timeout_ms);

  // Really does not expect to receive more than 65k of data in 1 batch
  assert(i_rx_bytes_cnd <= 0xFFFF);

  *pu_num_of_read_bytes = i_rx_bytes_cnd;

  return USC_OK;
}

te_usc_error_code usu_uart_write(const uint8_t* pu8_data,
                                 const uint16_t u16_data_length,
                                 const uint16_t u16_timeout_ms) {
  int i_tx_bytes =
      uart_write_bytes(UI_SERVICE_UART_INTERFACE, pu8_data, u16_data_length);

  if (i_tx_bytes != u16_data_length) {
    LOGE(tag, "Transmitted %d but expected to transmit %d", i_tx_bytes,
         u16_data_length);
    return USC_ERROR;
  }

  TickType_t ticks_to_wait = u16_timeout_ms / portTICK_PERIOD_MS;
  // Should not be zero. At least 1 tick should be given
  if (ticks_to_wait == 0) {
    ticks_to_wait = 1;
  }

  esp_err_t e_err_uart_hal =
      uart_wait_tx_done(UI_SERVICE_UART_INTERFACE, ticks_to_wait);

  if (e_err_uart_hal == ESP_ERR_TIMEOUT) {
    return USC_ERROR_TIMEOUT;

  } else if (e_err_uart_hal) {
    return USC_ERROR;

  } else {
    return USC_OK;
  }
}

te_usc_error_code usu_uart_deinit(void) {
  // Try to remove driver no matter what
  uart_driver_delete(UI_SERVICE_UART_INTERFACE);

  // Disable also GPIO
  gpio_pad_select_gpio(UI_SERVICE_TXD_PIN);
  gpio_pad_select_gpio(UI_SERVICE_RXD_PIN);

  gpio_config_t s_cfg = {
      .pin_bit_mask = BIT64(UI_SERVICE_TXD_PIN) | BIT64(UI_SERVICE_RXD_PIN),
      .mode = GPIO_MODE_DISABLE,
      .pull_up_en = false,
      .pull_down_en = false,
      .intr_type = GPIO_INTR_DISABLE,
  };
  gpio_config(&s_cfg);

  return USC_OK;
}

// ==========================| Internal functions |===========================
te_usc_error_code _read_array_i32(const char* pac_str,
                                  const uint8_t u8_num_of_items,
                                  int32_t* pi32_buffer, const int32_t i32_min,
                                  const int32_t i32_max) {
  assert(pac_str);
  assert(u8_num_of_items);
  assert(pi32_buffer);

  te_usc_error_code e_err_code =
      usu_str_dec_array_to_int_32(pac_str, u8_num_of_items, pi32_buffer);

  // If conversion fails, tell why
  if (e_err_code) {
    usc_printf("%s %s\n", usc_txt_fail(), usc_error_code_to_string(e_err_code));
    return e_err_code;
  }

  // Check values in buffer. They shall be in required range
  bool b_invalid_value_detected = false;
  for (uint8_t u8_item_idx = 0; u8_item_idx < u8_num_of_items; u8_item_idx++) {
    if ((pi32_buffer[u8_item_idx] < i32_min) ||
        (pi32_buffer[u8_item_idx] > i32_max)) {
      usc_printf("%s %d - value out range. Expected value %d ~ %d\n",
                 usc_txt_fail(), pi32_buffer[u8_item_idx], i32_min, i32_max);

      b_invalid_value_detected = true;
      // Keep checking inside loop, in order to report user all problems
    }
  }

  // Overwrite error code no matter what
  if (b_invalid_value_detected) {
    e_err_code = USC_ERROR_INVALID_PARAMETER;
  }

  return e_err_code;
}
