/**
 * @file
 * @author Martin Stejskal
 * @brief User/project specific module for UI service code
 */
#ifndef __UI_SERVICE_USER_H__
#define __UI_SERVICE_USER_H__
// ===============================| Includes |================================
#include <esp_err.h>

#include "ui_service_core.h"
// ================================| Defines |================================
#define UI_SERVICE_USER_CAN_TIMEOUT_MS (100)

/**
 * @brief When executing "factory reset" command, this delay is applied
 *
 * Value is in seconds. Just give user some time to read fact that system will
 * be rebooted
 */
#define UI_SERVICE_USER_FACTORY_RESET_REBOOT_TIMEOUT_SEC (5)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
const ts_usc_cmd_cfg* ui_service_user_get_list_of_cmds(void);
uint16_t ui_service_user_get_num_of_cmds(void);

// ===============================| Commands |================================

void ui_service_cmd_get_pwm(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_pwm(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_get_ir_addr(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_ir_addr(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_get_ir_cmd_list(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_get_ir_cmd(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_ir_cmd(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_get_dac_inc(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_get_dac_dec(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_dac_inc(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_dac_dec(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_get_pwm_step_profile(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_pwm_step_thresholds(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_pwm_step_steps(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_get_temp(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_get_overheat_params(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_overheat_temp(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_overheat_pwm(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_overheat_time(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_get_mag_thrshld_ut(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_mag_thrshld_ut(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_get_mag_ampliture_ut(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_get_stable_angle_cnt(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_stable_angle_cnt(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_get_mag_deadzone_deg(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_mag_deadzone_deg(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_get_mag_zero_pos_tol_deg(ts_usc_cb_args* ps_cb_args);
void ui_service_cmd_set_mag_zero_pos_tol_deg(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_factory_reset(ts_usc_cb_args* ps_cb_args);

void ui_service_cmd_reboot(ts_usc_cb_args* ps_cb_args);

// ===========================| Interface related |===========================
te_usc_error_code usu_uart_init(void);

te_usc_error_code usu_uart_read(uint8_t* pu8_data,
                                const uint16_t u16_buffer_length,
                                const uint16_t u16_timeout_ms,
                                uint16_t* pu_num_of_read_bytes);

te_usc_error_code usu_uart_write(const uint8_t* pu8_data,
                                 const uint16_t u16_data_length,
                                 const uint16_t u16_timeout_ms);

te_usc_error_code usu_uart_deinit(void);
#endif  // __UI_SERVICE_USER_H__
