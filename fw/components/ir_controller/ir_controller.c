/**
 * @file
 * @author Martin Stejskal
 * @brief IR controller
 *
 * Deals with IR sensors, send events when detected object close to sensors,
 * setup HW and so on.
 */
// ===============================| Includes |================================
#include "ir_controller.h"

#include <driver/adc.h>
#include <driver/dac.h>
#include <driver/rmt.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>
#include <string.h>

#include "cfg.h"
#include "ir_tools.h"
#include "log_system.h"
#include "nvs_settings.h"
// ================================| Defines |================================
/**
 * @brief Default value for DAC
 *
 * The 90 worked well in my case. Sensors were reacting only few cm from
 * transmitter. However it really depends on HW. It should be fine-tuned
 * on every HW.
 */
#define _DAC_DEFAULT (95)

/**
 * @brief Define maximum response time to "keep running" change in RX task
 *
 * Value is in milliseconds.
 */
#define _RX_TASK_BLOCK_MAX_MS (1000)

/**
 * @brief Buffer for IR receiver in Bytes
 *
 * The 512 is not enough. You can easily face issue with full buffer.
 */
#define _IR_BUFFER_IN_BYTES (1024)

#define _GET_NUM_OF_ITEMS(array) (sizeof(array) / sizeof(array[0]))
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

typedef struct {
  // IR address for device
  uint16_t u16_addr;

  // List of commands and their values. List follows teIrCmds
  uint8_t au8_cmd[IR_CNTRL_CMD_MAX];
} ts_ir_cmd_list;

typedef struct {
  uint8_t au8_dac[IR_CNTRL_FNC_MAX];
  ts_ir_cmd_list cmd;
} ts_nvs;

typedef enum {
  RMT_DIR_RX = 0,
  RMT_DIR_TX = 1,

  RMT_DIR_MAX,
} te_rmt_direction;

// Preset default values
static const ts_nvs ms_nvs_default = {
    .au8_dac =
        {
            _DAC_DEFAULT,
            _DAC_DEFAULT,
        },
    .cmd =
        {
            .u16_addr = IR_CNTRLR_DEFAULT_ADDR,

            // Note: order have to match with tsIrCntrlFunction enumeration
            .au8_cmd = {IR_CNTRLR_DEFAULT_CMD_INC, IR_CNTRLR_DEFAULT_CMD_DEC,
                        IR_CNTRLR_DEFAULT_CMD_INC_FAST,
                        IR_CNTRLR_DEFAULT_CMD_DEC_FAST,
                        IR_CNTRLR_DEFAULT_CMD_TIMER,
                        IR_CNTRLR_DEFAULT_CMD_POWER},
        },
};

typedef struct {
  // Have 2 RX channels. Every channel for one function
  RingbufHandle_t h_ring_buffer[IR_CNTRL_FNC_MAX];

  // Every function have own builder and parser
  ir_builder_t *h_builder[IR_CNTRL_FNC_MAX];
  ir_parser_t *h_parser[IR_CNTRL_FNC_MAX];

  SemaphoreHandle_t h_process_event_semaphore;

  bool b_keep_running;

  // When some event/action is recognized, it will callback user defined
  // function (if not empty)
  tp_event_cb pf_event_callback;

  // Thing that are also stored in NVS, but we need them for run
  ts_nvs s_nvs;
} ts_ir_cntrl_vars;
// ===========================| Global variables |============================
static ts_ir_cntrl_vars ms_ir_cntrls_var;

static char *tag = "IR controller";
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void _rx_task_rtos(void *pv_args);
static void _tx_task_rtos(void *pv_args);
// ==============| Internal function prototypes: middle level |===============
static void _process_event(uint16_t u16_address, uint16_t u16_cmd);

static void _on_exit_rx_task(const ts_ir_cntrl_function e_function);
static void _on_exit_tx_task(const ts_ir_cntrl_function e_function);

static esp_err_t _load_settings(void);
static esp_err_t _save_settings(void);

static void _get_event_from_cmd_value(uint8_t u8_cmd, te_event_type *pe_event,
                                      uint8_t *pu8_repeat);
static uint16_t _get_light_intensity(const ts_ir_cntrl_function e_function);
// ================| Internal function prototypes: low level |================
static esp_err_t _setup_rmt(void);

static void _setup_builders_and_parsers(void);

static rmt_channel_t _get_rmt_channel(const ts_ir_cntrl_function e_function,
                                      const te_rmt_direction e_direction);
static te_ir_cmds _get_cmd_idx_from_function(
    const ts_ir_cntrl_function e_function);

static uint8_t _get_gpio_num(const ts_ir_cntrl_function e_function,
                             const te_rmt_direction e_direction);
static esp_err_t _setup_adc(void);
static esp_err_t _setup_dac(void);
static esp_err_t _set_dac(const ts_ir_cntrl_function e_function,
                          const uint8_t u8_raw);
// =========================| High level functions |==========================
void ir_controller_init(const tp_event_cb pf_event_cb) {
  // Pass parameter
  ms_ir_cntrls_var.pf_event_callback = pf_event_cb;

  esp_err_t e_err_code = _load_settings();

  if (e_err_code) {
    // Although this is problem, device is still functional. Let's just print
    // error, but continue
    LOGE(tag, "Loading data from NVS failed: %s", esp_err_to_name(e_err_code));
  }

  // Setup remote controller itself (GPIO, bitrate, modulation, etc), DAC
  e_err_code = _setup_rmt() | _setup_dac() | _setup_adc();
  if (e_err_code) {
    // If this does not works, then running whole task is pointless
    LOGE(tag, "Initialization of remote controller HW failed: %s",
         esp_err_to_name(e_err_code));
    return;
  }

  _setup_builders_and_parsers();

  // Need to set this flag to allow keep running tasks below
  ms_ir_cntrls_var.b_keep_running = true;

  // If handler to semaphore is not created yet, create it
  if (!ms_ir_cntrls_var.h_process_event_semaphore) {
    ms_ir_cntrls_var.h_process_event_semaphore = xSemaphoreCreateBinary();

    // Make semaphore available
    xSemaphoreGive(ms_ir_cntrls_var.h_process_event_semaphore);
  }

  // Every function have own channel. Create linear list (passed to tasks, so it
  // have to be stored somewhere)
  ts_ir_cntrl_function e_func_list[IR_CNTRL_FNC_MAX];
  for (ts_ir_cntrl_function e_cnt = (ts_ir_cntrl_function)0;
       e_cnt < IR_CNTRL_FNC_MAX; e_cnt++) {
    e_func_list[e_cnt] = e_cnt;

    // Pass to the task current function. It is passed pointer, so value should
    // be actually somewhere stored even though it is linear list of numbers
    xTaskCreate(_rx_task_rtos, "IR RX", 2 * 1024, &e_func_list[e_cnt],
                IR_CONTROLLER_RX_TASK_PRIORITY, NULL);

    xTaskCreate(_tx_task_rtos, "IR TX", 2 * 1024, &e_func_list[e_cnt],
                IR_CONTROLLER_RX_TASK_PRIORITY, NULL);
  }
}
// ========================| Middle level functions |=========================
void ir_controller_restore_defaults(void) {
  nvs_settings_set(tag, &ms_nvs_default, sizeof(ms_nvs_default));

  // Update also variables in RAM -> apply all changes "immediately"
  _load_settings();
}
// ==========================| Low level functions |==========================
uint16_t ir_controller_get_address(void) {
  return ms_ir_cntrls_var.s_nvs.cmd.u16_addr;
}

esp_err_t ir_controller_set_address(uint16_t u16_address) {
  // Apply and store in NVS
  ms_ir_cntrls_var.s_nvs.cmd.u16_addr = u16_address;

  return _save_settings();
}

uint8_t ir_controller_get_cmd_value(te_ir_cmds e_cmd_id) {
  // Typically the 0x00 is actually used. So 0xFF kind of mean "invalid
  // command". So this value will be returned in case that event is invalid
  uint8_t u8_cmd = 0xFF;

  if (e_cmd_id < IR_CNTRL_CMD_MAX) {
    // Valid input value
    u8_cmd = ms_ir_cntrls_var.s_nvs.cmd.au8_cmd[e_cmd_id];

  } else {
    // Invalid input value -> keep command value
  }

  return u8_cmd;
}

esp_err_t ir_controller_set_cmd_value(te_ir_cmds e_cmd_id,
                                      uint8_t u8_cmd_value) {
  esp_err_t e_err_code = ESP_FAIL;

  if (e_cmd_id >= IR_CNTRL_CMD_MAX) {
    // Invalid range - keep error code set to fail
  } else {
    ms_ir_cntrls_var.s_nvs.cmd.au8_cmd[e_cmd_id] = u8_cmd_value;

    e_err_code = _save_settings();
  }

  return e_err_code;
}

uint8_t ir_controller_get_dac(ts_ir_cntrl_function e_function) {
  if (e_function >= IR_CNTRL_FNC_MAX) {
    // Invalid input
    return 0;
  }

  return ms_ir_cntrls_var.s_nvs.au8_dac[e_function];
}

esp_err_t ir_controller_set_dac(ts_ir_cntrl_function e_function,
                                uint8_t u8_new_dac_value) {
  if (e_function >= IR_CNTRL_FNC_MAX) {
    return ESP_ERR_INVALID_ARG;
  }

  // Store value locally, so it can be easily stored in NVS
  ms_ir_cntrls_var.s_nvs.au8_dac[e_function] = u8_new_dac_value;

  // Set value actually on HW peripheral
  _set_dac(e_function, u8_new_dac_value);

  return _save_settings();
}

const char *ir_controller_get_cmd_name(const te_ir_cmds e_cmd_id) {
  switch (e_cmd_id) {
    case IR_CNTRL_CMD_INC:
      return "Inc";
    case IR_CNTRL_CMD_DEC:
      return "Dec";
    case IR_CNTRL_CMD_INC_FAST:
      return "Inc fast";
    case IR_CNTRL_CMD_DEC_FAST:
      return "Dec fast";
    case IR_CNTRL_CMD_TIMER:
      return "Timer";
    case IR_CNTRL_CMD_POWER:
      return "Power";
    case IR_CNTRL_CMD_MAX:
      // Actually invalid value
      return NULL;
  }

  // Unexpected case
  return NULL;
}

const char *ir_controller_get_function_name(
    const ts_ir_cntrl_function e_function) {
  switch (e_function) {
    case IR_CNTRL_FNC_INC:
      return "increase";
    case IR_CNTRL_FNC_DEC:
      return "decrease";
    case IR_CNTRL_FNC_MAX:
      // Actually invalid value
      return NULL;
  }

  // Unexpected case
  return NULL;
}
// ====================| Internal functions: high level |=====================
static void _rx_task_rtos(void *pv_args) {
  // Function is given as parameter
  const ts_ir_cntrl_function e_function = *((ts_ir_cntrl_function *)pv_args);

  rmt_item32_t *p_rx_items = NULL;
  uint32_t u32_length = 0;

  // Is it repeated request?
  bool b_repeat;

  // Received address and command
  uint32_t u32_address, u32_cmd;

  while (ms_ir_cntrls_var.b_keep_running) {
    // In theory, we could just set maximum timeout and that would be it. But
    // in case that "keep running" will be disabled, task would be stuck for
    // a while -> in order to have at least some practical response time, let's
    // "go around" bit more often
    p_rx_items = (rmt_item32_t *)xRingbufferReceive(
        ms_ir_cntrls_var.h_ring_buffer[e_function], &u32_length,
        pdMS_TO_TICKS(_RX_TASK_BLOCK_MAX_MS));

    // Process only if content is not empty. Other wise "do nothing" and wait
    if (p_rx_items) {
      u32_length /= 4;  // One RMT = 4 Bytes

      if (ms_ir_cntrls_var.h_parser[e_function]->input(
              ms_ir_cntrls_var.h_parser[e_function], p_rx_items, u32_length) ==
          ESP_OK) {
        if (ms_ir_cntrls_var.h_parser[e_function]->get_scan_code(
                ms_ir_cntrls_var.h_parser[e_function], &u32_address, &u32_cmd,
                &b_repeat) == ESP_OK) {
          // Hurray - possible to parse

          // Regarding to the command, same story as in case TX. Parser will
          // simply read it from LSB to MSB, which actually swap intended Byte
          // order
          u32_cmd = ((u32_cmd & 0xFF) << 8) | ((u32_cmd >> 8) & 0xFF);

          LOGV(tag, "Scan Code %s --- addr: 0x%04x cmd: 0x%04x",
               b_repeat ? "(repeat)" : "", u32_address, u32_cmd);

          // The address and command should be only 16 bit long ideally. So
          // check this before processing
          if ((u32_cmd > 0xFFFF) || (u32_cmd > 0xFFFF)) {
            LOGW(tag, "Address and command are over 16 bit long!");
          } else {
            // Address and command are 16 bit long -> ready to be processed
            _process_event((uint16_t)u32_address, (uint16_t)u32_cmd);
          }
        }
      } else {
        // Dump data
        LOGV(tag, "unknown data");
      }
      // After parsing the data, return spaces to ring buffer.
      vRingbufferReturnItem(ms_ir_cntrls_var.h_ring_buffer[e_function],
                            (void *)p_rx_items);
    }

    // Perform following code "always"
    // Just check that reading from photo resistor works
    LOGV(tag, "ADC value for %s: %d",
         ir_controller_get_function_name(e_function),
         _get_light_intensity(e_function));
  }

  _on_exit_rx_task(e_function);
}

static void _tx_task_rtos(void *pv_args) {
  // Function is given as parameter
  const ts_ir_cntrl_function e_function = *((ts_ir_cntrl_function *)pv_args);

  // In order to access command from list, index is needed -> get it from
  // function value
  const te_ir_cmds e_cmd_idx = _get_cmd_idx_from_function(e_function);

  rmt_item32_t *p_tx_items = NULL;

  // Builder need to store packet length somewhere
  uint32_t u32_length;

  // Using extended version - address is 16 bit long
  uint16_t u16_addr;

  // Loaded command
  uint16_t u16_cmd;

  // To simplify things, just get pointer to command structure
  const ts_ir_cmd_list *ps_cmd = &ms_ir_cntrls_var.s_nvs.cmd;

  while (ms_ir_cntrls_var.b_keep_running) {
    // Allow to change value "on the fly" -> need to load every time
    u16_addr = ps_cmd->u16_addr;

    // Command is 8 bit, but higher part is regular value, while lower part is
    // inverted. But: builder will just take lowest 16 bits and transmit them
    // from LSB to MSB. Therefore it actually swap Byte order
    u16_cmd = ((0xFF & (~ps_cmd->au8_cmd[e_cmd_idx])) << 8) |
              (ps_cmd->au8_cmd[e_cmd_idx] & 0xFF);

    LOGV(tag, "IR TX. Addr: 0x%x Cmd: 0x%x", u16_addr,
         ps_cmd->au8_cmd[e_cmd_idx]);

    ESP_ERROR_CHECK(ms_ir_cntrls_var.h_builder[e_function]->build_frame(
        ms_ir_cntrls_var.h_builder[e_function], u16_addr, u16_cmd));

    ESP_ERROR_CHECK(ms_ir_cntrls_var.h_builder[e_function]->get_result(
        ms_ir_cntrls_var.h_builder[e_function], &p_tx_items, &u32_length));
    // To send data according to the waveform items. The "false" states to "non
    // blocking mode"
    rmt_write_items(_get_rmt_channel(e_function, RMT_DIR_TX), p_tx_items,
                    u32_length, false);

    DELAY_MS(IR_TX_PERIOD_MS);
  }

  _on_exit_tx_task(e_function);
}
// ===================| Internal functions: middle level |====================
static void _process_event(uint16_t u16_address, uint16_t u16_cmd) {
  // Track time for every valid event in order to detect duplicates (from IR
  // device like TV remote or phone)
  static uint32_t u32_last_valid_event_ms = 0;
  static te_event_type e_last_valid_event = EVENT_TYPE_NONE;

  // The address can be any 16 bit value. So simply compare if address is for us
  // or not
  if (u16_address != ms_ir_cntrls_var.s_nvs.cmd.u16_addr) {
    // IR request is for another device
    return;
  }

  // Get current time in milliseconds
  uint32_t u32_current_time_ms = GET_CLK_MS_32BIT();

  // Command is basically 8 bit value. Higher 8 bits are normal, but lower 8
  // bits are inverted. Check that before further processing
  uint8_t u8_cmd = u16_cmd >> 8;
  uint8_t u8_cmd_inv = (0xFF & u16_cmd);

  // Invert inverted part -> then it should match with non-inverted one.
  // Note: it did not worked in "if" condition for some reason. The bit
  // inversion had to be done within extra step. Maybe compiler automatically
  // cast values to 32 bit value and then it make mess.
  u8_cmd_inv = ~u8_cmd_inv;

  if (u8_cmd != u8_cmd_inv) {
    // Command is corrupted
    return;
  }

  te_event_type e_event;
  uint8_t u8_repeat;
  _get_event_from_cmd_value(u8_cmd, &e_event, &u8_repeat);

  // Here comes some trouble due to RTOS nature. This function should
  // be called only "once at the time", because it allow detect
  // duplicate events. However we can hardly tell when RTOS decide to
  // pause this task. So semaphore have to be used
  xSemaphoreTake(ms_ir_cntrls_var.h_process_event_semaphore, portMAX_DELAY);

  // Check if event was recognized
  if (e_event != EVENT_TYPE_NONE) {
    if ((e_event == e_last_valid_event) &&
        ((u32_current_time_ms - u32_last_valid_event_ms) < IR_TX_PERIOD_MS)) {
      // It is duplicate event -> ignore it
      LOGD(tag, "Event %s IGNORED", event_get_name(e_event));
    } else {
      // Worth to process
      LOGV(tag, "Event: %s", event_get_name(e_event));

      // Check if callback is set. If yes, call it
      if (ms_ir_cntrls_var.pf_event_callback) {
        // If needed, call callback multiple times
        for (uint8_t u8_exec_cnt = 0; u8_exec_cnt <= u8_repeat; u8_exec_cnt++) {
          ms_ir_cntrls_var.pf_event_callback(e_event);
        }
      }

      u32_last_valid_event_ms = u32_current_time_ms;
      e_last_valid_event = e_event;
    }
  }

  xSemaphoreGive(ms_ir_cntrls_var.h_process_event_semaphore);
}

static void _on_exit_rx_task(const ts_ir_cntrl_function e_function) {
  // Uninstall all RMT driver
  rmt_driver_uninstall(_get_rmt_channel(e_function, RMT_DIR_RX));

  // De-allocate parser
  ms_ir_cntrls_var.h_parser[e_function]->del(
      ms_ir_cntrls_var.h_parser[e_function]);

  vTaskDelete(0);
}

static void _on_exit_tx_task(const ts_ir_cntrl_function e_function) {
  rmt_driver_uninstall(_get_rmt_channel(e_function, RMT_DIR_TX));

  // De-allocate builder
  ms_ir_cntrls_var.h_builder[e_function]->del(
      ms_ir_cntrls_var.h_builder[e_function]);

  vTaskDelete(0);
}

static esp_err_t _load_settings(void) {
  te_nvs_sttngs_err e_err_code =
      nvs_settings_get(tag, &ms_ir_cntrls_var.s_nvs, &ms_nvs_default,
                       sizeof(ms_ir_cntrls_var.s_nvs));

  if (e_err_code) {
    return ESP_FAIL;
  }

  return ESP_OK;
}

static esp_err_t _save_settings(void) {
  te_nvs_sttngs_err e_err_code = nvs_settings_set(
      tag, &ms_ir_cntrls_var.s_nvs, sizeof(ms_ir_cntrls_var.s_nvs));

  if (e_err_code) {
    return ESP_FAIL;
  } else {
    return ESP_OK;
  }
}

static uint16_t _get_light_intensity(const ts_ir_cntrl_function e_function) {
  // Light intensity in Lux units (approximate)
  uint16_t u16_light_lux = 0;

  // Keeps error code from ADC
  esp_err_t e_err_code = ESP_FAIL;

  int i_adc_out = 0;

  switch (e_function) {
    case IR_CNTRL_FNC_INC:
      e_err_code = adc2_get_raw(IO_LGHT_SENS_INC, ADC_WIDTH_BIT_10, &i_adc_out);
      break;
    case IR_CNTRL_FNC_DEC:
      e_err_code = adc2_get_raw(IO_LGHT_SENS_DEC, ADC_WIDTH_BIT_10, &i_adc_out);
      break;
    default:
      // This should not happen
      assert(0);
  }

  if (e_err_code) {
    // Something went wrong. Keep default value in u16_light_lux
  } else {
    // Reading from ADC is OK. Recalculate to Lux
    ///@todo Recalculate to Lux
    u16_light_lux = (uint16_t)i_adc_out;
  }

  return u16_light_lux;
}

static void _get_event_from_cmd_value(uint8_t u8_cmd, te_event_type *pe_event,
                                      uint8_t *pu8_repeat) {
  // Address and command seems to be fine. Let's check if there is any event
  // assigned to them
  const uint8_t *pu8_cmd_list = ms_ir_cntrls_var.s_nvs.cmd.au8_cmd;

  // In case that command will not match to any known command, this value stay
  // unchanged
  *pe_event = EVENT_TYPE_NONE;
  // By default, assume that event is not repeated
  *pu8_repeat = 0;

  if (u8_cmd == pu8_cmd_list[IR_CNTRL_CMD_INC]) {
    *pe_event = EVENT_TYPE_INC_BRIGHTNESS;

  } else if (u8_cmd == pu8_cmd_list[IR_CNTRL_CMD_DEC]) {
    *pe_event = EVENT_TYPE_DEC_BRIGHTNESS;

  } else if (u8_cmd == pu8_cmd_list[IR_CNTRL_CMD_INC_FAST]) {
    *pe_event = EVENT_TYPE_INC_BRIGHTNESS;
    *pu8_repeat = IR_CNTRLR_INC_DEC_FAST_NUM_OF_STEPS;

  } else if (u8_cmd == pu8_cmd_list[IR_CNTRL_CMD_DEC_FAST]) {
    *pe_event = EVENT_TYPE_DEC_BRIGHTNESS;
    *pu8_repeat = IR_CNTRLR_INC_DEC_FAST_NUM_OF_STEPS;

  } else if (u8_cmd == pu8_cmd_list[IR_CNTRL_CMD_TIMER]) {
    *pe_event = EVENT_TYPE_TIMER;

  } else if (u8_cmd == pu8_cmd_list[IR_CNTRL_CMD_POWER]) {
    *pe_event = EVENT_TYPE_POWER_TOGGLE;

  } else {
    // Unknown command - keep event "none"
  }
}
// =====================| Internal functions: low level |=====================
static esp_err_t _setup_rmt(void) {
  esp_err_t e_err_code = ESP_FAIL;

  // Here is standard settings. A lot of hard-coded values, but basically this
  // is commonly used.
  rmt_config_t s_rmt_tx_config = {
      .rmt_mode = RMT_MODE_TX,
      .channel = RMT_CHANNEL_MAX,  // Will be filled later
      .gpio_num = 0,               // Will be filled later
      .clk_div = 80,
      .mem_block_num = 1,
      .flags = 0,
      .tx_config = {
          .carrier_freq_hz = 38000,
          .carrier_level = RMT_CARRIER_LEVEL_HIGH,
          .idle_level = RMT_IDLE_LEVEL_LOW,
          .carrier_duty_percent = 33,
          .carrier_en = true,
          .loop_en = false,
          .idle_output_en = true,
      }};

  rmt_config_t s_rmt_rx_config = {
      .rmt_mode = RMT_MODE_RX,
      .channel = RMT_CHANNEL_MAX,  // Will be filled later
      .gpio_num = 0,               // Will be filled later
      .clk_div = 80,
      .mem_block_num = 1,
      .flags = 0,
      .rx_config = {
          .idle_threshold = 12000,
          .filter_ticks_thresh = 100,
          .filter_en = true,
      }};

  // Setup and install RMT drivers
  for (ts_ir_cntrl_function e_function = (ts_ir_cntrl_function)0;
       e_function < IR_CNTRL_FNC_MAX; e_function++) {
    // Setup channel and GPIO
    s_rmt_rx_config.channel = _get_rmt_channel(e_function, RMT_DIR_RX);
    s_rmt_rx_config.gpio_num = _get_gpio_num(e_function, RMT_DIR_RX);

    s_rmt_tx_config.channel = _get_rmt_channel(e_function, RMT_DIR_TX);
    s_rmt_tx_config.gpio_num = _get_gpio_num(e_function, RMT_DIR_TX);

    // =============================| Configure RX |==========================
    e_err_code = rmt_config(&s_rmt_rx_config);
    if (e_err_code) {
      break;
    }

    e_err_code = rmt_driver_install(_get_rmt_channel(e_function, RMT_DIR_RX),
                                    _IR_BUFFER_IN_BYTES, 0);
    if (e_err_code) {
      break;
    }

    e_err_code =
        rmt_get_ringbuf_handle(_get_rmt_channel(e_function, RMT_DIR_RX),
                               &ms_ir_cntrls_var.h_ring_buffer[e_function]);
    if (e_err_code) {
      break;
    }

    // Pointer should not be empty
    assert(ms_ir_cntrls_var.h_ring_buffer[e_function]);

    // Reset memory index (avoid transmitter to send random data)
    e_err_code = rmt_rx_start(_get_rmt_channel(e_function, RMT_DIR_RX), true);

    // =============================| Configure TX |==========================
    e_err_code = rmt_config(&s_rmt_tx_config);
    if (e_err_code) {
      break;
    }

    e_err_code =
        rmt_driver_install(_get_rmt_channel(e_function, RMT_DIR_TX), 0, 0);
    if (e_err_code) {
      break;
    }
  }

  return e_err_code;
}

static void _setup_builders_and_parsers(void) {
  // Pre-configured builder and parser configuration structures. RMT channel
  // will be set later
  ir_builder_config_t s_builder_cfg = {
      .buffer_size = 64,          // Default in example. Works well, so why not
      .dev_hdl = (ir_dev_t)NULL,  // Will be changed later
      .flags = IR_TOOLS_FLAGS_PROTO_EXT,  // Extended protocol enabled
  };

  ir_parser_config_t s_parser_cfg = {
      .dev_hdl = (ir_dev_t)NULL,          // Will be changed later,
      .flags = IR_TOOLS_FLAGS_PROTO_EXT,  // Extended protocol enabled
      .margin_us = 200,  // Default value in example. Works well, so why not
  };

  for (ts_ir_cntrl_function e_function = (ts_ir_cntrl_function)0;
       e_function < IR_CNTRL_FNC_MAX; e_function++) {
    // Setup builder
    s_builder_cfg.dev_hdl = (ir_dev_t)_get_rmt_channel(e_function, RMT_DIR_TX);
    ms_ir_cntrls_var.h_builder[e_function] =
        ir_builder_rmt_new_nec(&s_builder_cfg);

    // Setup parser
    s_parser_cfg.dev_hdl = (ir_dev_t)_get_rmt_channel(e_function, RMT_DIR_RX);
    ms_ir_cntrls_var.h_parser[e_function] =
        ir_parser_rmt_new_nec(&s_parser_cfg);
  }
}

inline static rmt_channel_t _get_rmt_channel(
    const ts_ir_cntrl_function e_function, const te_rmt_direction e_direction) {
  assert(e_function < IR_CNTRL_FNC_MAX);
  assert(e_direction < RMT_DIR_MAX);

  // Target is to increase channel periodically after every direction
  return (rmt_channel_t)(e_function * RMT_DIR_MAX) + e_direction;
}

inline static te_ir_cmds _get_cmd_idx_from_function(
    const ts_ir_cntrl_function e_function) {
  if (e_function >= IR_CNTRL_FNC_MAX) {
    // Out of range -> return "MAX" which signalize that something is wrong
    return IR_CNTRL_CMD_MAX;
  } else {
    // Order is actually same for values lower than MAX -> can directly
    // re-assign
    return (te_ir_cmds)e_function;
  }
}

static uint8_t _get_gpio_num(const ts_ir_cntrl_function e_function,
                             const te_rmt_direction e_direction) {
  assert(e_function < IR_CNTRL_FNC_MAX);
  assert(e_direction < RMT_DIR_MAX);

  uint8_t u8_gpio_num = 0;

  switch (e_function) {
    case IR_CNTRL_FNC_INC:
      switch (e_direction) {
        case RMT_DIR_RX:
          u8_gpio_num = IO_IR_IN_INC;
          break;

        case RMT_DIR_TX:
          u8_gpio_num = IO_IR_MOD_OUT_INC;
          break;

        default:
          // Should not happen
          assert(0);
      }
      break;

    case IR_CNTRL_FNC_DEC:
      switch (e_direction) {
        case RMT_DIR_RX:
          u8_gpio_num = IO_IR_IN_DEC;
          break;

        case RMT_DIR_TX:
          u8_gpio_num = IO_IR_MOD_OUT_DEC;
          break;

        default:
          // Should not happen
          assert(0);
      }
      break;

    default:
      // Should not happen
      assert(0);
  }

  return u8_gpio_num;
}

static esp_err_t _setup_adc(void) {
  /* For now, just use fixed attenuation. But possible to switch at runtime
   * in order to get more accurate results.
   */
  esp_err_t e_err_code =
      adc2_config_channel_atten(IO_LGHT_SENS_INC, ADC_ATTEN_DB_2_5);
  e_err_code |= adc2_config_channel_atten(IO_LGHT_SENS_DEC, ADC_ATTEN_DB_2_5);

  return e_err_code;
}

static esp_err_t _setup_dac(void) {
  esp_err_t e_err_code = ESP_FAIL;

  // The DAC is mapped to channel, not to GPIO. Therefore defines for those
  // GPIO are basically pointless.
  e_err_code = dac_output_enable(IO_IR_PWR_LED_INC);
  e_err_code |= dac_output_enable(IO_IR_PWR_LED_DEC);

  e_err_code |= _set_dac(IR_CNTRL_FNC_INC,
                         ms_ir_cntrls_var.s_nvs.au8_dac[IR_CNTRL_FNC_INC]);
  e_err_code |= _set_dac(IR_CNTRL_FNC_DEC,
                         ms_ir_cntrls_var.s_nvs.au8_dac[IR_CNTRL_FNC_DEC]);

  return e_err_code;
}

static esp_err_t _set_dac(const ts_ir_cntrl_function e_function,
                          const uint8_t u8_raw) {
  dac_channel_t e_channel = DAC_CHANNEL_MAX;

  switch (e_function) {
    case IR_CNTRL_FNC_INC:
      e_channel = IO_IR_PWR_LED_INC;
      break;
    case IR_CNTRL_FNC_DEC:
      e_channel = IO_IR_PWR_LED_DEC;
      break;
    default:
      // This should not happen
      assert(0);
  }

  return dac_output_voltage(e_channel, u8_raw);
}
