/**
 * @file
 * @author Martin Stejskal
 * @brief IR controller
 *
 * Deals with IR sensors, send events when detected object close to sensors,
 * setup HW and so on.
 */
#ifndef __IR_CONTROLLER_H__
#define __IR_CONTROLLER_H__
// ===============================| Includes |================================
#include <esp_err.h>
#include <stdint.h>

#include "events.h"
// ================================| Defines |================================
/**
 * @brief Default IR address for device
 */
#define IR_CNTRLR_DEFAULT_ADDR (0xBEEF)

/**
 * @brief Default IR commands
 *
 * Following values assign function to them. The default values should follow
 * some IR standard, so eventually should be possible to use remote controller
 * if address would be properly modified.
 *
 * @{
 */

/**
 * @brief IR command - increase light intensity
 */
#define IR_CNTRLR_DEFAULT_CMD_INC (0x04)

/**
 * @brief IR command - decrease light intensity
 */
#define IR_CNTRLR_DEFAULT_CMD_DEC (0x00)

/**
 * @brief IR command - increase light intensity rapidly
 */
#define IR_CNTRLR_DEFAULT_CMD_INC_FAST (0x31)

/**
 * @brief IR command - decrease light intensity rapidly
 */
#define IR_CNTRLR_DEFAULT_CMD_DEC_FAST (0x32)

/**
 * @brief IR command - timer (toggle)
 */
#define IR_CNTRLR_DEFAULT_CMD_TIMER (0x15)

/**
 * @brief IR command - power toggle
 */
#define IR_CNTRLR_DEFAULT_CMD_POWER (0x07)

/**
 * @}
 */

/**
 * @brief How often IR diode transmitting in milliseconds
 *
 * This is kind of balance between system responsibility and performance.
 */
#define IR_TX_PERIOD_MS (100)

/**
 * @brief How many times regular event should be repeated in "fast" mode
 *
 * Lower layer can require to change brightness faster. For this there is
 * constant below, which actually define how many "regular events" is
 * equal this "fast event".
 * Example: value is set to 5. The 1 "fast incrementation event" will equal to
 * 5 "basic incrementation event".
 */
#define IR_CNTRLR_INC_DEC_FAST_NUM_OF_STEPS (6)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
/**
 * @brief List of basic functions provided by IR transmitter
 *
 * Number of items in this enum is equal to number of IR transmitters on board.
 * Typically upper layer do not need to worry about this.
 */
typedef enum {
  IR_CNTRL_FNC_INC = 0,  //!< Function for increasing brightness
  IR_CNTRL_FNC_DEC = 1,  //!< Function for decreasing brightness

  IR_CNTRL_FNC_MAX = 2  //!< Last item in enumeration
} ts_ir_cntrl_function;

/**
 * @brief List of available commands
 *
 * Commands are used internally and typically application layer do not need to
 * worry about them.
 */
typedef enum {
  IR_CNTRL_CMD_INC = 0,  /**< Increase brightness */
  IR_CNTRL_CMD_DEC,      /**< Decrease brightness */
  IR_CNTRL_CMD_INC_FAST, /**< Increase brightness fast */
  IR_CNTRL_CMD_DEC_FAST, /**< Decrease brightness fast */
  IR_CNTRL_CMD_TIMER,    /**< Execute timer function */
  IR_CNTRL_CMD_POWER,    /**< Power on/off command */

  IR_CNTRL_CMD_MAX /**< Last item in the list */
} te_ir_cmds;
// ===========================| Global variables |============================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief IR controller's task for RTOS
 *
 * Use this task to allow controller do it's job. It will spawn own RTOS tasks.
 *
 * @param pf_event_cb Pointer to event notification callback. Can be empty, but
 *        then it is meaningless to have this task at all.
 */
void ir_controller_init(const tp_event_cb pf_event_cb);
// ========================| Middle level functions |=========================
/**
 * @brief Restore default settings and apply it
 *
 * When you need to recover default settings. It will also automatically store
 * default settings into NVS.
 */
void ir_controller_restore_defaults(void);
// ==========================| Low level functions |==========================
/**
 * @brief Get current IR address used by controller
 *
 * The address helps to distinguish between IR devices.
 *
 * @return IR address as 16 bit long number
 */
uint16_t ir_controller_get_address(void);

/**
 * @brief Set new IR address and store in NVS
 *
 * If lamp is randomly mess with your TV, you might need to change address. The
 * address helps to distinguish between IR devices. Changing it should help
 * you.
 *
 * @param u16_address New IR address. Will be applied immediately
 * @return ESP_OK if no error
 */
esp_err_t ir_controller_set_address(uint16_t u16_address);

/**
 * @brief Get command value for given command ID
 *
 * Commands differentiate actions.
 *
 * @param e_cmd_id Command ID - one from enumeration
 * @return Command value used by IR transmitter and receiver
 */
uint8_t ir_controller_get_cmd_value(te_ir_cmds e_cmd_id);

/**
 * @brief Set command value for given command ID
 *
 * Commands differentiate actions.
 *
 * @param e_cmd_id Command ID - one from enumeration
 * @param u8_cmd_value Command value used by IR transmitter and receiver
 * @return ESP_OK if everything is fine
 */
esp_err_t ir_controller_set_cmd_value(te_ir_cmds e_cmd_id,
                                      uint8_t u8_cmd_value);

/**
 * @brief Get DAC value for given function
 *
 * @param e_function Use one of function ID. Any value above MAX will be
 *                   ignored and zero will be returned.
 * @return Raw DAC value for selected function
 */
uint8_t ir_controller_get_dac(ts_ir_cntrl_function e_function);

/**
 * @brief Update DAC value for given function
 *
 * IR LED transmitters have to be regulated. If they will transmit too much,
 * the IR receiver might receive signal even if there is no close object.
 * Too low value might cause that even close object is not detected. This
 * depends some fine tuning, since it depends on used HW.
 *
 * @param e_function One of function ID. Any value above MAX will cause
 *         returning ESP_ERR_INVALID_ARG error code.
 * @param u8_new_dac_value New DAC value in raw format
 * @return ESP_OK if no error
 */
esp_err_t ir_controller_set_dac(ts_ir_cntrl_function e_function,
                                uint8_t u8_new_dac_value);

// =============================| Print related |=============================
/**
 * @brief Get command ID as string
 * @param e_cmd_id Command ID as one value from enumeration
 * @return Pointer to string
 */
const char* ir_controller_get_cmd_name(const te_ir_cmds e_cmd_id);

/**
 * @brief Get function name as string
 *
 * @param e_function Function as one value from enumeration
 * @return Pointer to string
 */
const char* ir_controller_get_function_name(
    const ts_ir_cntrl_function e_function);

#endif  // __IR_CONTROLLER_H__
