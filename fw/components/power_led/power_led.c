/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for power LED
 */
// ===============================| Includes |================================
// Standard header files
// ESP's headers
// Kind of generic, but Espressif use it too
#include <driver/gpio.h>
#include <driver/ledc.h>
#include <esp32/rom/ets_sys.h>  // Precise delays
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>
#include <time.h>

// Custom headers
#include "cfg.h"
#include "log_system.h"
#include "nvs_settings.h"
#include "power_led.h"
// ================================| Defines |================================
/**
 * @brief Low or high speed of whole PWM module
 *
 * Typically when high frequencies are required, high speed mode is preferred
 */
#define PWR_LED_LEDC_SPEED (LEDC_LOW_SPEED_MODE)

/**
 * @brief Default shine after very first power on
 *
 * Note that this value is used only once in lifetime ideally. When user
 * change value, original value will be overwritten and later on written to
 * non-volatile memory -> this default value will not be used anymore, since
 * we always load value from non-volatile memory.
 *
 */
#define PWR_LED_DEFAULT_PWM (0xFF)

#define PWR_LED_NO_BLINK_1_MS (400)
#define PWR_LED_NO_BLINK_2_MS (400)
#define PWR_LED_NO_BLINK_TIMES (2)

// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum { PWR_LED_FADE_DIR_UP, PWR_LED_FADE_DIR_DOWN } teFadeDirection;

typedef struct {
  // Tells if module is initialized or not
  bool b_initialized;

  // Initial task still running
  bool b_fade_tsk_running;

  // Flag that suggest initial task to stop
  bool b_stop_fade_tsk;

  // Tells whether current PWM value should be stored into NVS or not. In some
  // cases it might re beneficial to disable this automatic saving (timer mode
  // for example)
  bool b_store_pwm_in_nvs;

  // Define maximum PWM value. This can be used as power limiter
  uint8_t u8_max_pwm;
} ts_pwr_led_var;
// ===========================| Global variables |============================
/**
 * @brief tag for logger
 */
static const char *tag = "PwrLED";

/* Setup timer. Here are few catches, so let's mention few of these:
 *  * Higher PWM frequency, faster fade effect have to be. Or it have to
 *    be implemented somehow here
 *  * Higher bit resolution -> slower PWM
 *  * The ledc_set_fade_with_time() still have some bugs, so let's not
 *    use it although it seems to be nice
 */
static const ledc_timer_config_t ms_ledc_timer_cfg = {
    // resolution of PWM duty cycle
    .duty_resolution = LEDC_TIMER_8_BIT,
    // frequency of PWM signal
    .freq_hz = 100000,
    // timer mode
    .speed_mode = PWR_LED_LEDC_SPEED,
    // timer index
    .timer_num = PWR_LED_PWM_TIMER,
    // Auto select the source clock
    .clk_cfg = LEDC_AUTO_CLK,
};

/*
 * Prepare configuration channel of LED Controller by selecting:
 * - controller's channel number
 * - output duty cycle, set initially to 0 (off)
 * - GPIO number where signal will be distributed
 * - speed mode, either high or low
 * - timer servicing selected channel
 *   Note: if different channels use one timer,
 *         then frequency and bit_num of these channels
 *         will be the same
 */
static const ledc_channel_config_t ms_ledc_channel_cfg = {
    .channel = PWR_LED_PWM_CH,
    .duty = 0,
    .gpio_num = IO_PWR_LED_PWM,
    .speed_mode = PWR_LED_LEDC_SPEED,
    .hpoint = 0,
    .timer_sel = PWR_LED_PWM_TIMER};

static ts_pwr_led_var ms_pwr_led_var = {
    .b_store_pwm_in_nvs = true,

    // By default allow full range
    .u8_max_pwm = 0xFF,
};
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static void _if_init_task_run_stop_it(void);

static void _set_pwm_raw(const uint8_t u8_pwm_value);

static void _log_not_initialized(void);
// =========================| High level functions |==========================
void pwr_led_init(void) {
  // Set configuration of timer
  ESP_ERROR_CHECK(ledc_timer_config(&ms_ledc_timer_cfg));

  ESP_ERROR_CHECK(ledc_channel_config(&ms_ledc_channel_cfg));

  ms_pwr_led_var.b_initialized = true;
}

void pwr_led_deinit(void) {
  gpio_pad_select_gpio(IO_PWR_LED_PWM);

  gpio_config_t s_cfg = {
      .pin_bit_mask = BIT64(IO_PWR_LED_PWM),
      .mode = GPIO_MODE_DISABLE,
      .pull_up_en = false,
      .pull_down_en = false,
      .intr_type = GPIO_INTR_DISABLE,
  };
  gpio_config(&s_cfg);

  ms_pwr_led_var.b_initialized = false;
}

void pwr_led_run_fade_task(void *pv_args) {
  // Module have to be initialized!
  if (!ms_pwr_led_var.b_initialized) {
    _log_not_initialized();

    // Kill this task: 0 - means self
    vTaskDelete(0);
    return;
  }

  // If some other fade effect is in progress, stop it gracefully
  while (ms_pwr_led_var.b_fade_tsk_running) {
    ms_pwr_led_var.b_stop_fade_tsk = 1;

    // Smallest possible delay
    DELAY_MS(1);
  }

  ms_pwr_led_var.b_fade_tsk_running = 1;
  ms_pwr_led_var.b_stop_fade_tsk = 0;

  LOGI(tag, "Running fade task");

  uint8_t u8_current_pwm = pwr_led_get_pwm();

  // Pre-load default values for case that no argument is passed
  ts_pwr_led_arg s_args = {.u8_target_pwm = UINT8_MAX,
                           .u32_duration_ms = 0,
                           .u16_step_ms = 60,
                           .p_finished_cb = NULL,
                           .p_no_change_cb = NULL};

  // If argument is given, expect that correct argument structure was passed
  if (pv_args) {
    memcpy(&s_args, pv_args, sizeof(s_args));
  }

  if (u8_current_pwm == s_args.u8_target_pwm) {
    // No change needed -> exit
    LOGI(tag, "Current and target PWM values are same. Exiting task");
    ms_pwr_led_var.b_fade_tsk_running = 0;

    if (s_args.p_no_change_cb) {
      LOGD(tag, "Executing no change callback");
      s_args.p_no_change_cb();
    }

    vTaskDelete(0);
    return;
  }

  // Increase bright according to the real time
  uint32_t u32_time_actual_ms = GET_CLK_MS_32BIT();

  // Define sleep interval between every change in ms
  uint32_t u32_sleep_step_ms;

  // Define when PWM value should be changed. This helps to deal with RTOS
  // inaccuracy. In order to make very first change "now", we push reference
  // time slightly back, so when processing inside while loop, it will be
  // evaluated that there is time to change PWM value
  uint32_t u32_next_run_ms = u32_time_actual_ms - 1;

  // Holds how long should main loop stay in sleep
  uint32_t u32_sleep_time_ms;

  teFadeDirection e_fade_direction;

  if (u8_current_pwm < s_args.u8_target_pwm) {
    e_fade_direction = PWR_LED_FADE_DIR_UP;

  } else {
    e_fade_direction = PWR_LED_FADE_DIR_DOWN;
  }

  // If step is defined, then use it
  if (s_args.u16_step_ms) {
    u32_sleep_step_ms = s_args.u16_step_ms;

  } else {
    // Sleep step have to be calculated here, not previously, in order to avoid
    // entering if condition above
    if (e_fade_direction == PWR_LED_FADE_DIR_UP) {
      u32_sleep_step_ms =
          (s_args.u32_duration_ms) / (s_args.u8_target_pwm - u8_current_pwm);

    } else if (e_fade_direction == PWR_LED_FADE_DIR_DOWN) {
      u32_sleep_step_ms =
          (s_args.u32_duration_ms) / (u8_current_pwm - s_args.u8_target_pwm);

    } else {
      assert(0);
    }
  }

  // ==============================| Fade effect |============================
  while (u8_current_pwm != s_args.u8_target_pwm) {
    // If received stop, get out from the fade loop
    if (ms_pwr_led_var.b_stop_fade_tsk) {
      LOGI(tag, "Fade interrupted by user");
      break;
    }

    u32_time_actual_ms = GET_CLK_MS_32BIT();
    if (u32_time_actual_ms < u32_next_run_ms) {
      // Wake up early. Already checked for stop flag, nothing more to do here

    } else {
      // It is up to time to change PWM value
      if (e_fade_direction == PWR_LED_FADE_DIR_UP) {
        u8_current_pwm++;
        /* Need to respect maximum PWM value. It can be changed at any time,
         * so it have to be checked every cycle
         */
        if (u8_current_pwm > ms_pwr_led_var.u8_max_pwm) {
          // Maximum reached -> exit
          u8_current_pwm = ms_pwr_led_var.u8_max_pwm;
          s_args.u8_target_pwm = u8_current_pwm;
        }
      } else {
        u8_current_pwm--;

        /* Going down, but still need to check if target PWM value will meet
         * requirement for maximum output power. If not, adjust it in sake of
         * safety.
         */
        if (s_args.u8_target_pwm > ms_pwr_led_var.u8_max_pwm) {
          s_args.u8_target_pwm = ms_pwr_led_var.u8_max_pwm;
        }
      }

      _set_pwm_raw(u8_current_pwm);

      u32_next_run_ms += u32_sleep_step_ms;
    }

    if (u32_next_run_ms <= u32_time_actual_ms) {
      // We're already late -> no delay
    } else {
      // Calculate sleep time. If over threshold, it is necessary to reduce it
      // in order to be able to react to stop request
      u32_sleep_time_ms = u32_next_run_ms - u32_time_actual_ms;

      // Sleep time should not be zero
      assert(u32_sleep_time_ms);

      // We need to make sure that sleep will not take too much time in order
      // to avoid "freezing" this task and having problems with aborting this
      // task
      if (u32_sleep_time_ms > PWR_LED_FADE_TASK_MAX_SLEEP_MS) {
        u32_sleep_time_ms = PWR_LED_FADE_TASK_MAX_SLEEP_MS;
      }

      // Here we have to be bit more practical. If delay is below some
      // threshold, the DELAY_MS() will be pretty inaccurate. There are 2 ways
      // to handle it. Increase PWM step or use different delay function. Every
      // have own advantages and disadvantages, but using different delay
      // function will actually make effect more smooth
      if (u32_sleep_time_ms < 20) {
        ets_delay_us(1000 * u32_sleep_time_ms);

      } else {
        DELAY_MS(u32_sleep_time_ms);
      }
    }
  }

  // Run callback if defined and if not aborted
  if (s_args.p_finished_cb && !ms_pwr_led_var.b_stop_fade_tsk) {
    LOGD(tag, "Executing finish callback");
    s_args.p_finished_cb();
  }

  LOGI(tag, "Fade finished");

  ms_pwr_led_var.b_fade_tsk_running = 0;

  // Kill this task: 0 - means self
  vTaskDelete(0);
}

void pwr_led_store_pwm_value_in_nvs(void *pv_args) {
  const uint8_t u8_default_pwm = PWR_LED_DEFAULT_PWM;
  uint8_t u8_loaded_pwm, u8_actual_pwm;

  while (1) {
    // Once per time, get PWM value and store it in NVS
    DELAY_MS(PWR_LED_STORE_PWM_VALUE_IN_NVM_S * 1000);

    if (!ms_pwr_led_var.b_store_pwm_in_nvs) {
      // For some reason was saving PWM value into NVS postponed. Skip this
      // round
      continue;
    }

    // Write only if value differs
    u8_actual_pwm = pwr_led_get_pwm();
    ESP_ERROR_CHECK(nvs_settings_get(tag, &u8_loaded_pwm, &u8_default_pwm,
                                     sizeof(u8_loaded_pwm)));

    if (u8_actual_pwm != u8_loaded_pwm) {
      LOGD(tag, "Updating PWM value in NVS: %d", u8_actual_pwm);
      ESP_ERROR_CHECK(
          nvs_settings_set(tag, &u8_actual_pwm, sizeof(u8_actual_pwm)));
    }
  }

  vTaskDelete(0);
}

// ========================| Middle level functions |=========================
void pwr_led_inc_bright(const uint8_t u8_inc_by) {
  if (!ms_pwr_led_var.b_initialized) {
    _log_not_initialized();
    return;
  }

  _if_init_task_run_stop_it();

  // Temporary variable which check range. have to be bigger than 8 bits
  // to detect overflow
  int16_t i16_tmp = (int16_t)pwr_led_get_pwm() + (int16_t)u8_inc_by;
  uint8_t u8_pwm_val;

  // Disallow to increase bright if overshoot allowed maximum
  if (i16_tmp > (int16_t)ms_pwr_led_var.u8_max_pwm) {
    // Blink "No"
    LOGD(tag, "Increase bright: No");

    // 1st PWM: MAX, 2nd PWM: half of maximum
    pwr_led_blink(ms_pwr_led_var.u8_max_pwm, ms_pwr_led_var.u8_max_pwm >> 1,
                  PWR_LED_NO_BLINK_1_MS, PWR_LED_NO_BLINK_2_MS,
                  PWR_LED_NO_BLINK_TIMES);

    // Set to maximum
    u8_pwm_val = ms_pwr_led_var.u8_max_pwm;
  } else {
    // Set what is desired
    u8_pwm_val = (uint8_t)i16_tmp;
  }
  _set_pwm_raw(u8_pwm_val);
}

void pwr_led_dec_bright(const uint8_t u8_dec_by) {
  if (!ms_pwr_led_var.b_initialized) {
    _log_not_initialized();
    return;
  }

  _if_init_task_run_stop_it();

  // Temporary variable which check range. have to be bigger than 8 bits
  // to detect overflow
  int16_t i16_tmp = (int16_t)pwr_led_get_pwm() - (int16_t)u8_dec_by;
  uint8_t u8_pwm_val;

  if (i16_tmp < 0) {
    // Blink "No"
    LOGD(tag, "Decrease bright: No");

    // 1st PWM: 0, 2nd PWM: 0x10 (magnitude up -> 2x in binary)
    pwr_led_blink(0, 0x10, PWR_LED_NO_BLINK_1_MS, PWR_LED_NO_BLINK_2_MS,
                  PWR_LED_NO_BLINK_TIMES);

    // Set to minimum
    u8_pwm_val = 0;
  } else {
    // Set what is desired
    u8_pwm_val = (uint8_t)i16_tmp;
  }
  _set_pwm_raw(u8_pwm_val);
}

void pwr_led_blink(const uint8_t u8_a_pwm, const uint8_t u8_b_pwm,
                   const uint16_t u16_a_delay_ms, const uint16_t u16_b_delay_ms,
                   const uint8_t u8_repeat) {
  for (uint8_t u8_repeat_cnt = 0; u8_repeat_cnt <= u8_repeat; u8_repeat_cnt++) {
    _set_pwm_raw(u8_a_pwm);
    if (u16_a_delay_ms) {
      DELAY_MS(u16_a_delay_ms);
    }

    _set_pwm_raw(u8_b_pwm);
    if (u16_b_delay_ms) {
      DELAY_MS(u16_b_delay_ms);
    }
  }
}

bool pwr_led_is_fade_task_running(void) {
  return ms_pwr_led_var.b_fade_tsk_running;
}

void pwr_led_restore_defaults(void) {
  const uint8_t u8_default_pwm = PWR_LED_DEFAULT_PWM;

  ESP_ERROR_CHECK(
      nvs_settings_set(tag, &u8_default_pwm, sizeof(u8_default_pwm)));
}

void pwr_led_set_pwm_maximum(const uint8_t u8_max_pwm_value) {
  ms_pwr_led_var.u8_max_pwm = u8_max_pwm_value;

  if (ms_pwr_led_var.b_fade_tsk_running) {
    // No need further action. Fade effect can already deal with this change
  } else {
    // Update PWM value if needed
    uint8_t u8_current_pwm = pwr_led_get_pwm();

    if (u8_current_pwm > u8_max_pwm_value) {
      pwr_led_set_pwm(u8_max_pwm_value);
    }
  }
}

uint8_t pwr_led_get_pwr_maximum(void) { return ms_pwr_led_var.u8_max_pwm; }
// ==========================| Low level functions |==========================
uint8_t pwr_led_get_pwm(void) {
  return ledc_get_duty(ms_ledc_channel_cfg.speed_mode,
                       ms_ledc_channel_cfg.channel);
}

uint8_t pwr_led_get_pwm_from_nvs(void) {
  const uint8_t u8_default_pwm = PWR_LED_DEFAULT_PWM;
  uint8_t u8_loaded_pwm;

  ESP_ERROR_CHECK(nvs_settings_get(tag, &u8_loaded_pwm, &u8_default_pwm,
                                   sizeof(u8_loaded_pwm)));

  return u8_loaded_pwm;
}

void pwr_led_set_pwm(const uint8_t u8_pwm_value) {
  if (!ms_pwr_led_var.b_initialized) {
    _log_not_initialized();
    return;
  }

  _if_init_task_run_stop_it();

  if (u8_pwm_value > ms_pwr_led_var.u8_max_pwm) {
    // Input value is higher than required limit -> use limit value
    _set_pwm_raw(ms_pwr_led_var.u8_max_pwm);

  } else {
    // Input value is fully valid
    _set_pwm_raw(u8_pwm_value);
  }
}

void pwr_led_abort_fade(void) { _if_init_task_run_stop_it(); }

void pwr_led_set_store_pwm_to_nvs(const bool b_store_into_nvs) {
  LOGI(tag, "Store to NVS: %d", b_store_into_nvs);

  ms_pwr_led_var.b_store_pwm_in_nvs = b_store_into_nvs;
}

bool pwr_led_get_store_pwm_to_nvs(void) {
  return ms_pwr_led_var.b_store_pwm_in_nvs;
}
// ==========================| Internal functions |===========================

static void _if_init_task_run_stop_it(void) {
  // If background task is still running, stop it
  if (ms_pwr_led_var.b_fade_tsk_running) {
    ms_pwr_led_var.b_stop_fade_tsk = 1;

    // Define timeout. This can help to diagnose problems if any
    uint32_t u32_current_time_ms = GET_CLK_MS_32BIT();
    const uint32_t u32_timeout_ms =
        u32_current_time_ms + (PWR_LED_FADE_TASK_MAX_SLEEP_MS * 2);

    while (ms_pwr_led_var.b_fade_tsk_running) {
      // Smallest possible delay
      DELAY_MS(1);

      u32_current_time_ms = GET_CLK_MS_32BIT();

      // Should not happen, but you never know
      if (u32_current_time_ms > u32_timeout_ms) {
        ESP_ERROR_CHECK(ESP_ERR_TIMEOUT);
      }
    }
  }
}

static void _set_pwm_raw(const uint8_t u8_pwm_value) {
  ESP_ERROR_CHECK(ledc_set_duty(ms_ledc_channel_cfg.speed_mode,
                                ms_ledc_channel_cfg.channel, u8_pwm_value));
  ESP_ERROR_CHECK(ledc_update_duty(ms_ledc_channel_cfg.speed_mode,
                                   ms_ledc_channel_cfg.channel));
}

static void _log_not_initialized(void) { LOGW(tag, "Module not initialized"); }
