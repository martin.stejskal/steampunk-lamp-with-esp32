/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for power LED
 */
#ifndef __POWER_LED_H__
#define __POWER_LED_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================
/**
 * @brief Define maximum latency between sending stop request and performing it
 *
 * The fade task does not need to use CPU most of the time. However they might
 * receive request to stop at any time. In some cases sleep time in task can be
 * in seconds, but typically we can not wait so long. So task will actually
 * wake up more often than needed, but just because of checking stop flag.
 * Lower value allow task to react faster on stop event, but it consume more
 * CPU time. High value increase "lag", but reduce CPU usage. Choose it wisely.
 */
#define PWR_LED_FADE_TASK_MAX_SLEEP_MS (100)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================

/**
 * @brief Define callback for notification that fade effect finished
 */
typedef void (*ts_fade_done_cb)(void);

/**
 * @brief Argument structure for @ref pwrLedRunFadeTask
 */
typedef struct {
  // Target PWM value
  uint8_t u8_target_pwm;

  // Duration in milliseconds
  uint32_t u32_duration_ms;

  // Alternatively it is possible to define step in milliseconds. If you want
  // to define total duration, keep this value zero. Otherwise set this value
  // and duration in seconds will be completely ignored. If both variables are
  // defined, this one will take precedence.
  uint16_t u16_step_ms;

  // When fade task finish, it can call notification function
  ts_fade_done_cb p_finished_cb;

  // When target PWM and current match, no fake effect is played
  ts_fade_done_cb p_no_change_cb;
} ts_pwr_led_arg;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Initialize power LED HW
 */
void pwr_led_init(void);

/**
 * @brief Deinitialize power LED HW
 */
void pwr_led_deinit(void);

/**
 * @brief RTOS task for running fade in effect
 * @param pv_args Pointer to arguments. Should be @ref tsPwrLedArg type
 */
void pwr_led_run_fade_task(void *pv_args);

/**
 * @brief RTOS task for writing PWM value to NVS so value can be recovered
 *
 * During power off all values in RAM are erased, so it is good idea to store
 * last value and recover it later on
 *
 * @param pv_args Pointer to arguments (no arguments processed)
 */
void pwr_led_store_pwm_value_in_nvs(void *pv_args);
// ========================| Middle level functions |=========================
/**
 * @brief Increase brightness by defined value
 *
 * When overflow would occur, driver would blink and keep higher possible value
 *
 * @note When fade in effect is ongoing and this function is called, effect
 *       is canceled
 *
 * @param u8_inc_by Increment value
 */
void pwr_led_inc_bright(const uint8_t u8_inc_by);

/**
 * @brief Decrease brightness by defined value
 *
 * When overflow would occur, driver would blink and keep lowest possible
 * value
 *
 * @note When fade in effect is ongoing and this function is called, effect
 *       is canceled
 *
 * @param u8_dec_by Decrement value
 */
void pwr_led_dec_bright(const uint8_t u8_dec_by);

/**
 * @brief Blink effect
 *
 * Blocking function, therefore it should be short event. Sequence is
 * following:
 *   * A phase (PWM A, delay A)
 *   * B phase (PWM B, delay B)
 *   * Repeat N times
 *
 * @param u8_a_pwm PWM value for first phase
 * @param u8_b_pwm PWM value for second phase
 * @param u16_a_delay_ms Delay for first phase
 * @param u16_b_delay_ms Delay for second phase
 * @param u8_repeat How many times should be sequence repeated
 */
void pwr_led_blink(const uint8_t u8_a_pwm, const uint8_t u8_b_pwm,
                   const uint16_t u16_a_delay_ms, const uint16_t u16_b_delay_ms,
                   const uint8_t u8_repeat);

/**
 * @brief Tells if fade task is running or not
 * @return True if fade task is running, false otherwise
 */
bool pwr_led_is_fade_task_running(void);

/**
 * @brief Restore default settings
 *
 * Set PWM value in NVS to default.
 */
void pwr_led_restore_defaults(void);

/**
 * @brief Set maximum allowed PWM value
 *
 * This can be used as output limiter. Once set, it will immediately cut output
 * power if needed. This can be used for case that device is overheating for
 * instance. By default is maximum 255 (8 bit limit) - full range.
 *
 * @param u8_max_pwm_value New allowed maximum
 */
void pwr_led_set_pwm_maximum(const uint8_t u8_max_pwm_value);

/**
 * @brief Returns maximum allowed PWM value
 *
 * Get function for @ref pwr_led_set_pwm_maximum
 *
 * @return Maximum allowed PWM value
 */
uint8_t pwr_led_get_pwr_maximum(void);
// ==========================| Low level functions |==========================
/**
 * @brief Return actual PWM value
 *
 * @note When fade in effect is ongoing and this function is called, effect
 *       is canceled
 *
 * @return PWM value as unsigned integer
 */
uint8_t pwr_led_get_pwm(void);

/**
 * @brief Returns PWM value stored in NVS
 *
 * This might be useful after reboot, when application want to restore
 * previously used value
 *
 * @return PWM value as unsigned integer
 */
uint8_t pwr_led_get_pwm_from_nvs(void);

/**
 * @brief Set PWM value directly
 *
 * @note When fade in effect is ongoing and this function is called, effect
 *       is canceled
 *
 * @param u8_pwm_value PWM value as unsigned integer
 */
void pwr_led_set_pwm(const uint8_t u8_pwm_value);

/**
 * @brief Explicitly abort fade effect
 *
 * This function is not require to be called in following scenarios, because
 * internal logic will handle abort:
 *  * Fade is running, but there is request to change PWM (any function)
 *  * Fade is running, but another fade request is raised.
 *
 * This is suitable only in cases, that fade should be aborted when used as
 * timer for example.
 */
void pwr_led_abort_fade(void);

/**
 * @brief Allow to enable/disable storing current PWM into NVS
 *
 * When @ref pwr_led_store_pwm_value_in_nvs task is running, by default it will
 * periodically store current PWM value into NVS. Sometimes it might we desired
 * to postpone this process. To do so, simply call this function with
 * appropriate argument.
 *
 * @param b_store_into_nvs If true, values will be periodically stored into
 *                         NVS. Otherwise nothing will be stored into NVS.
 */
void pwr_led_set_store_pwm_to_nvs(const bool b_store_into_nvs);

/**
 * @brief Read "store enable" status
 *
 * @return True if storing to NVS is enabled, false otherwise
 */
bool pwr_led_get_store_pwm_to_nvs(void);

#endif  // __POWER_LED_H__
