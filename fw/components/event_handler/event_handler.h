/**
 * @file
 * @author Martin Stejskal
 * @brief Event handler module
 *
 * Deals with events from sensors, process them and send simplified commands
 * to application layer
 */
#ifndef __EVENT_HANDLER_H__
#define __EVENT_HANDLER_H__
// ===============================| Includes |================================
#include "events.h"
#include "ir_controller.h"
// ================================| Defines |================================
/**
 * @brief Time window to consider both IR "buttons to be pressed"
 *
 * IR sensors are technically not working in parallel. Basically every IR
 * sensor have own task -> not even synchronized. Job of this value is to
 * determine if both sensors are "pressed" simultaneously. Using in
 * consideration 2 samples per every measurement should cover this.
 */
#define EVENT_HNDLR_IR_BOTH_PRESSED_WINDOW_MS (IR_TX_PERIOD_MS * 2)

/**
 * @brief When "both" are detected, ignore events from IR sensor
 *
 * Point is to avoid sending "timer on/off" in series. When "both" IR events
 * are detected, it is simply better to not process new events from IR sensors
 * for a while. Value is in ms.
 */
#define EVENT_HNDLR_IR_BOTH_PRESSED_IGNORE_TIME_MS (2000)

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Task for event handler module
 * @param pv_args Expected callback function @ref tp_event_handler_cb
 */
void event_handler_task_rtos(void* pv_args);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __EVENT_HANDLER_H__
