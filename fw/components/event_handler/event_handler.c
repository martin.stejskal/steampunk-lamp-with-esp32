/**
 * @file
 * @author Martin Stejskal
 * @brief Event handler module
 *
 * Deals with events from sensors, process them and send simplified commands
 * to application layer
 */
// ===============================| Includes |================================
#include "event_handler.h"

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>
#include <string.h>

#include "cfg.h"
#include "ir_controller.h"
#include "log_system.h"
#include "sensors_controller.h"
// ================================| Defines |================================
#define _EVENT_QUEUE_NUM_OF_ITEMS (20)

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  EVENT_H_SRC_IR_CONTROLLER,
  EVENT_H_SRC_SENSORS_CONTROLLER,
  EVENT_H_SRC_MAX,
} te_event_source;

typedef struct {
  uint32_t u32_time_msec;
  te_event_type e_event_type;
  te_event_source e_src_type;
} ts_event_item;

typedef struct {
  // Queue from callbacks to main event handler task
  QueueHandle_t h_event_queue;

  // Callback for passing information to application layer
  tp_event_cb ps_event_callback;
} ts_event_handler_vars;
// ===========================| Global variables |============================
static char *tag = "Event handler";

static ts_event_handler_vars ms_event_h_vars;
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
/**
 * @brief Callback function for IR sensors
 * @param e_event Specific event type
 */
static void _ir_event_cb(te_event_type e_event);
static void _sensor_event_cb(te_event_type e_event);
// ================| Internal function prototypes: low level |================
static void _send_event_from_callback_to_main(const ts_event_item s_event_app,
                                              const uint8_t u8_repeat,
                                              const uint32_t u32_timeout_ms);
// =========================| High level functions |==========================
void event_handler_task_rtos(void *pv_args) {
  ms_event_h_vars.ps_event_callback = (tp_event_cb)pv_args;

  // Create queue which holds processed events from callback functions. Note
  // that queue have to be created before start other tasks in order to avoid
  // assertion error
  ms_event_h_vars.h_event_queue =
      xQueueCreate(_EVENT_QUEUE_NUM_OF_ITEMS, sizeof(ts_event_item));
  assert(ms_event_h_vars.h_event_queue);

  // Start sensor tasks
  ir_controller_init(_ir_event_cb);
  sensors_cntrlr_init(_sensor_event_cb);

  BaseType_t e_queue_err_code;
  ts_event_item s_event;

  LOGI(tag, "Event handler setup and running");

  uint32_t u32_current_time_ms;

  while (1) {
    // Wait for
    e_queue_err_code =
        xQueueReceive(ms_event_h_vars.h_event_queue, &s_event, portMAX_DELAY);

    u32_current_time_ms = GET_CLK_MS_32BIT();

    if (e_queue_err_code == pdTRUE) {
      // Different processing for different source of events
      if (EVENT_H_SRC_IR_CONTROLLER == s_event.e_src_type) {
        // If event happen more than TX period, it does not make sense to
        // process
        if ((u32_current_time_ms - s_event.u32_time_msec) >= IR_TX_PERIOD_MS) {
          // old information. Do not process
          continue;
        }
      } else if (EVENT_H_SRC_SENSORS_CONTROLLER == s_event.e_src_type) {
        /* In case that sensors is keep sending "increase light" and
         * application recognize that output power is already at maximum,
         * it will start blinking and after that it will accept more events.
         * Problem happen when during this blinking events would accumulate,
         * but their timestamp are out dated. In that case only meaningful
         * events are "power on" and "power off" -> these has to be
         * processed. Events "increase light" and "decrease light" can be
         * discarded.
         */
        if ((u32_current_time_ms - s_event.u32_time_msec) >=
            SENSORS_CNTRLR_MAG_PERIOD_MS) {
          if ((EVENT_TYPE_INC_BRIGHTNESS == s_event.e_event_type) ||
              (EVENT_TYPE_DEC_BRIGHTNESS == s_event.e_event_type)) {
            // These events can be discarded
            continue;
          }
        }

      } else {
        // Unexpected source event. Should not happen
        assert(0);
      }

      // Something was really received. If callback is set, call it
      if (ms_event_h_vars.ps_event_callback) {
        ms_event_h_vars.ps_event_callback(s_event.e_event_type);
      }

    } else {
      // Queue timeout - nothing to process
    }
  }

  vTaskDelete(0);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
static void _ir_event_cb(te_event_type e_event) {
  // When both events are detected "simultaneously" (within some window), it
  // is necessary to ignore upcoming events for some time
  static uint32_t u32_ignore_events_up_to_ms = 0;

  // Need to store previous event, so advanced functionality can be detected
  static ts_event_item s_prev_event;

  ts_event_item s_curr_event = {
      .u32_time_msec = GET_CLK_MS_32BIT(),
      .e_event_type = e_event,
      .e_src_type = EVENT_H_SRC_IR_CONTROLLER,
  };

  // How many times should be event repeatedly pushed to queue. Default is zero,
  // which means that event will be pushed to queue only once
  uint8_t u8_repeat = 0;

  if (u32_ignore_events_up_to_ms >= s_curr_event.u32_time_msec) {
    // This event should be ignored
    return;
  }

  // Event for application layer. Due to possible combinations it is possible
  // that this event might mean actually something else
  ts_event_item s_event_app = {
      .u32_time_msec = s_curr_event.u32_time_msec,
      .e_event_type = EVENT_TYPE_NONE,
  };

  /* There are 3 options:
   *   * Only "increase" event
   *   * Only "decrease" event
   *   * Both of them
   *
   * The third is tricky to detect. It is basically necessary to compare
   * previously registered event. And if current and previous differ (means
   * "increase" and "decrease") were registered within some time window, it
   * means this it 3rd option.
   */
  if (((s_curr_event.u32_time_msec - s_prev_event.u32_time_msec) <
       EVENT_HNDLR_IR_BOTH_PRESSED_WINDOW_MS) &&
      (s_curr_event.e_event_type != s_prev_event.e_event_type) &&
      (s_prev_event.e_event_type != EVENT_TYPE_NONE)) {
    // Detected "both"
    s_event_app.e_event_type = EVENT_TYPE_TIMER;

    // And ignore all events for some time
    u32_ignore_events_up_to_ms =
        s_curr_event.u32_time_msec + EVENT_HNDLR_IR_BOTH_PRESSED_IGNORE_TIME_MS;

  } else {
    // Simply assign received event
    s_event_app.e_event_type = e_event;
  }

  assert(s_event_app.e_event_type != EVENT_TYPE_NONE);

  // Send event
  _send_event_from_callback_to_main(s_event_app, u8_repeat, IR_TX_PERIOD_MS);

  // Current became previous
  memcpy(&s_prev_event, &s_curr_event, sizeof(ts_event_item));
}

static void _sensor_event_cb(te_event_type e_event) {
  // How many times should be event repeatedly pushed to queue. Default is zero,
  // which means that event will be pushed to queue only once
  uint8_t u8_repeat = 0;

  ts_event_item s_event_app = {
      .u32_time_msec = GET_CLK_MS_32BIT(),
      .e_event_type = e_event,
      .e_src_type = EVENT_H_SRC_SENSORS_CONTROLLER,
  };

  LOGD(tag, "Sensor event received: %s", event_get_name(e_event));

  // Send event
  _send_event_from_callback_to_main(s_event_app, u8_repeat,
                                    SENSORS_CNTRLR_MAG_PERIOD_MS);
}
// =====================| Internal functions: low level |=====================
static void _send_event_from_callback_to_main(const ts_event_item s_event_app,
                                              const uint8_t u8_repeat,
                                              const uint32_t u32_timeout_ms) {
  for (uint8_t u8_cnt = 0; u8_cnt <= u8_repeat; u8_cnt++) {
    xQueueSend(ms_event_h_vars.h_event_queue, &s_event_app,
               pdMS_TO_TICKS(u32_timeout_ms));
  }
}
