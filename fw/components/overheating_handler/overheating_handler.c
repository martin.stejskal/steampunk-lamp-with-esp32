/**
 * @file
 * @author Martin Stejskal
 * @brief Handles overheating problems
 */
// ===============================| Includes |================================
#include "overheating_handler.h"

#include <assert.h>
#include <driver/gpio.h>

#include "cfg.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef struct {
  // Configuration from upper layer
  const ts_overheating_hander_cfg *pf_cfg;

  // Latest registered "overheating" event
  uint32_t u32_overheating_event_ms;
  bool b_overheating_timestamp_valid;

  /* When no new "overheating" event comes within this period, it is assumed
   * that device cool down and it is safe to switch to the normal state.
   */
  uint32_t u32_cool_down_period_ms;

  /* Keep track when stage was change */
  uint32_t u32_stage_changed_ms;

  // Tells if module was initialized properly or not
  bool b_initialized;
} ts_runtime_vars;
// ===========================| Global variables |============================
static ts_runtime_vars ms_overheat_runtime_vars;
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static te_overheating_handler_stages _stage_0(void);

static te_overheating_handler_stages _stage_1_4(
    const uint32_t u32_since_last_event_ms, const uint32_t u32_current_time_ms,
    te_overheating_handler_stages e_stage);

static te_overheating_handler_stages _stage_5(
    const uint32_t u32_since_last_event_ms);
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
te_overheating_h_err_code overheating_handler_init(
    const ts_overheating_hander_cfg *const ps_cfg) {
  // Empty pointer can not be tolerated
  if (!ps_cfg) {
    return OVERHEATING_H_ERR_CODE_EMPTY_POINTER;
  }

  /* Assume that "overheating event" is at least 15x smaller than maximum
   * possible value in 32 bit range. Reason is that we need to multiply
   * this input value by 1.5x, but without using float. And only way is to
   * multiply number by 15 and then divide by 10.
   */
  if (ps_cfg->u32_temperature_sensor_period_ms >= (((1ULL << 32) - 1) / 15)) {
    return OVERHEATING_H_ERR_CODE_INVALID_ARGUMENT;
  }

  // Setup signal LED, disable it for now (no overheating reported yet)
  const gpio_config_t s_gpio_cfg = {
      // Disable interrupt
      .intr_type = GPIO_INTR_DISABLE,
      .mode = GPIO_MODE_OUTPUT,
      .pin_bit_mask = BIT64(IO_OVERHEATING_LED),
      .pull_down_en = 0,
      .pull_up_en = 0,
  };
  gpio_config(&s_gpio_cfg);
  gpio_set_level(IO_OVERHEATING_LED, 0);

  ms_overheat_runtime_vars.pf_cfg = ps_cfg;

  /* Calculate cool down period as 1.5x of sensor period. Since it is integer,
   * fractional multiplication is not directly possible -> x15 / 10
   */
  ms_overheat_runtime_vars.u32_cool_down_period_ms =
      (ps_cfg->u32_temperature_sensor_period_ms * 15) / 10;

  ms_overheat_runtime_vars.b_initialized = true;

  return OVERHEATING_H_ERR_CODE_OK;
}

uint8_t overheating_handler_task(void) {
  // States for state machine
  static te_overheating_handler_stages e_stage = OVERHEATING_H_STAGE_0;

  // Module not initialized -> tells that there is something wrong
  if (!ms_overheat_runtime_vars.b_initialized) {
    return 0;
  }

  const uint32_t u32_current_time_ms = GET_CLK_MS_32BIT();

  // Measure time since last known overheating event
  const uint32_t u32_since_last_event_ms =
      u32_current_time_ms - ms_overheat_runtime_vars.u32_overheating_event_ms;

  te_overheating_handler_stages e_new_stage = OVERHEATING_H_STAGE_5;

  if (OVERHEATING_H_STAGE_0 == e_stage) {
    e_new_stage = _stage_0();

  } else if ((e_stage >= OVERHEATING_H_STAGE_1) &&
             (e_stage <= OVERHEATING_H_STAGE_4)) {
    e_new_stage =
        _stage_1_4(u32_since_last_event_ms, u32_current_time_ms, e_stage);

  } else {
    assert(OVERHEATING_H_STAGE_5 == e_stage);

    e_new_stage = _stage_5(u32_since_last_event_ms);
  }

  if (e_new_stage > OVERHEATING_H_STAGE_0) {
    gpio_set_level(IO_OVERHEATING_LED, 1);
  } else {
    gpio_set_level(IO_OVERHEATING_LED, 0);
  }

  // If stage changed, record when
  if (e_new_stage != e_stage) {
    ms_overheat_runtime_vars.u32_stage_changed_ms = u32_current_time_ms;
    e_stage = e_new_stage;
  }

  return ms_overheat_runtime_vars.pf_cfg->au8_pwm_profile[e_new_stage];
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
void overheating_handler_overheating_event(const uint32_t u32_time_stamp_ms) {
  ms_overheat_runtime_vars.u32_overheating_event_ms = u32_time_stamp_ms;
  ms_overheat_runtime_vars.b_overheating_timestamp_valid = true;
}
// ====================| Internal functions: high level |=====================
static te_overheating_handler_stages _stage_0(void) {
  te_overheating_handler_stages e_stage = OVERHEATING_H_STAGE_0;

  if (ms_overheat_runtime_vars.b_overheating_timestamp_valid) {
    // Detected overheating event -> change state
    e_stage = OVERHEATING_H_STAGE_1;
  } else {
    // No overheating event -> stay here
  }

  return e_stage;
}

static te_overheating_handler_stages _stage_1_4(
    const uint32_t u32_since_last_event_ms, const uint32_t u32_current_time_ms,
    te_overheating_handler_stages e_stage) {
  assert((e_stage >= OVERHEATING_H_STAGE_1) &&
         (e_stage <= OVERHEATING_H_STAGE_4));

  // No overheating event -> no limitation anymore
  if (u32_since_last_event_ms >
      ms_overheat_runtime_vars.u32_cool_down_period_ms) {
    e_stage = OVERHEATING_H_STAGE_0;
    ms_overheat_runtime_vars.b_overheating_timestamp_valid = false;

  } else {
    // Overheating is still there
    uint32_t u32_in_this_state_ms =
        u32_current_time_ms - ms_overheat_runtime_vars.u32_stage_changed_ms;

    if (u32_in_this_state_ms >=
        (1000 *
         ms_overheat_runtime_vars.pf_cfg->au16_time_profile_sec[e_stage])) {
      // Increase stage
      e_stage++;
    }
  }

  return e_stage;
}

static te_overheating_handler_stages _stage_5(
    const uint32_t u32_since_last_event_ms) {
  te_overheating_handler_stages e_stage = OVERHEATING_H_STAGE_5;

  // No overheating event -> no limitation anymore
  if (u32_since_last_event_ms >
      ms_overheat_runtime_vars.u32_cool_down_period_ms) {
    e_stage = OVERHEATING_H_STAGE_0;
    ms_overheat_runtime_vars.b_overheating_timestamp_valid = false;

  } else {
    // Overheating is still there - can not do anything more now
  }

  return e_stage;
}
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
