/**
 * @file
 * @author Martin Stejskal
 * @brief Handles overheating problems
 */
#ifndef __OVERHEATING_HANDER_H__
#define __OVERHEATING_HANDER_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  OVERHEATING_H_ERR_CODE_OK = 0,
  OVERHEATING_H_ERR_CODE_FAIL,
  OVERHEATING_H_ERR_CODE_EMPTY_POINTER,
  OVERHEATING_H_ERR_CODE_INVALID_ARGUMENT,
} te_overheating_h_err_code;

/**
 * @brief List of possible stages
 */
typedef enum {
  OVERHEATING_H_STAGE_0 = 0, /**< Normal state. Full functionality */
  OVERHEATING_H_STAGE_1,     /**< Short overheating. Limit output power bit */
  OVERHEATING_H_STAGE_2,     /**< Limit output power bit more */
  OVERHEATING_H_STAGE_3,     /**< Limit output power more*/
  OVERHEATING_H_STAGE_4,     /**< Limit output power significantly */
  OVERHEATING_H_STAGE_5, /**< Output power at minimum value to avoid damage */

  OVERHEATING_H_STAGE_MAX, /**< Last value in list*/
} te_overheating_handler_stages;

/**
 * @brief Configuration structure for overheating module
 */
typedef struct {
  /* Define how long will system stay in current stage if overheating events
   * will be still going. Point is to limit output power "slowly", since
   * overheating might be just temporary or reducing output power by bit might
   * solve problem, so it is not necessary to dramatically reduce output power.
   * Note that very first and last values are irrelevant.
   * Value is in seconds.
   */
  uint16_t au16_time_profile_sec[OVERHEATING_H_STAGE_MAX];

  /* Every profile have to have defined PWM values. These values are used when
   * switching between profiles.
   */
  uint8_t au8_pwm_profile[OVERHEATING_H_STAGE_MAX];

  /* Tells module how often can "overheating" events can be send from
   * temperature sensor. Value does not have to be accurate. Algorithm counts
   * with some variation.
   */
  uint32_t u32_temperature_sensor_period_ms;
} ts_overheating_hander_cfg;
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
te_overheating_h_err_code overheating_handler_init(
    const ts_overheating_hander_cfg *const ps_cfg);
uint8_t overheating_handler_task(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
void overheating_handler_overheating_event(const uint32_t u32_time_stamp_ms);

#endif  // __OVERHEATING_HANDER_H__
