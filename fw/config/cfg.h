/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for Steampunk lamp
 *
 * Mainly here is HW configuration
 */
#ifndef __CFG_H__
#define __CFG_H__
// ===============================| Includes |================================
#include <driver/adc.h>
#include <esp_log.h>

#include "sensors_common.h"
// ================================| Defines |================================
/**
 * @brief Protocol CPU
 */
#define PRO_CPU (0)

/**
 * @brief Application CPU
 */
#define APP_CPU (1)
// ==================================| I/O |==================================
/**
 * @brief I2C GPIO mapping
 *
 * @{
 */
///@brief I2C data
#define IO_I2C_SDA (21)

///@brief I2C clock
#define IO_I2C_SCL (22)

/**
 * @}
 */

/**
 * @brief PWM output for power LED
 */
#define IO_PWR_LED_PWM (19)

/**
 * @brief IR related
 *
 * @{
 */

/**
 * @brief Power supply for IR LED - increasing action
 *
 * GPIO25
 *
 * @note Used DAC directly, but just to have it documented
 */
#define IO_IR_PWR_LED_INC (DAC_CHANNEL_1)

/**
 * @brief Power supply for IR LED - decreasing action
 *
 * GPIO26
 *
 * @note Used DAC directly, but just to have it documented
 */
#define IO_IR_PWR_LED_DEC (DAC_CHANNEL_2)

/**
 * @brief Modulated output for increasing action IR LED
 */
#define IO_IR_MOD_OUT_INC (32)

/**
 * @brief Modulated output for decreasing action IR LED
 */
#define IO_IR_MOD_OUT_DEC (33)

/**
 * @brief Demodulated input from "increase action" sensor
 */
#define IO_IR_IN_INC (36)

/**
 * @brief Demodulated input from "decrease action" sensor
 */
#define IO_IR_IN_DEC (39)

/**
 * @brief Light sensor for increase functionality
 *
 * IR module can use it in order to correct DAC value on IR LED according to
 * the outer light intensity.
 */
#define IO_LGHT_SENS_INC (ADC2_CHANNEL_0)

/**
 * @brief Light sensor for decrease functionality
 *
 * IR module can use it in order to correct DAC value on IR LED according to
 * the outer light intensity.
 */
#define IO_LGHT_SENS_DEC (ADC2_CHANNEL_7)

/**
 * @}
 */

/**
 * @brief LED signalize overheating problem
 */
#define IO_OVERHEATING_LED (2)

// ==============================| Service UI |===============================
/**
 * @brief Service UI IO
 *
 * @{
 */
/**
 * @brief Service UI baudrate
 */
#define UI_SERVICE_UART_BAUDRATE (115200)

/**
 * @brief Service UI UART interface
 */
#define UI_SERVICE_UART_INTERFACE (UART_NUM_1)

/**
 * @brief Service UI TX pin
 */
#define UI_SERVICE_TXD_PIN (GPIO_NUM_17)

/**
 * @brief Service UI RX pin
 */
#define UI_SERVICE_RXD_PIN (GPIO_NUM_16)
/**
 * @}
 */

/**
 * @brief Interface buffer size in Bytes
 *
 * Usually commands should be really short, so something like 256 should be
 * more than enough.
 *
 * @note For some reason, size under 128 is not allowed. Just be warned
 */
#define UI_SERVICE_ITF_BUFFER_SIZE (512)

// ============================| Task priorities |============================
/**
 * @brief Task priorities
 *
 * Higher number means higher priority
 *
 * @{
 */
#define APP_TASK_PRIORITY (4)
#define UI_SERVICE_CORE_TASK_PRIORITY (2)
#define EVENT_HANDLER_TASK_PRIORITY (6)
#define PWR_LED_STORE_IN_NVS_PRIORITY (1)
#define PWR_LED_FADE_EFFECT_PRIORITY (5)
#define IR_CONTROLLER_RX_TASK_PRIORITY (8)
#define SENSORS_CNTRLR_MAG_TASK_PRIORITY (7)
#define SENSORS_CNTRLR_TEMP_TASK_PRIORITY (3)
/**
 * @}
 */
// ===============================| Power LED |===============================
/**
 * @brief PWM channel for power LED
 */
#define PWR_LED_PWM_CH (LEDC_CHANNEL_0)

/**
 * @brief Timer for generating PWM for power LED
 */
#define PWR_LED_PWM_TIMER (LEDC_TIMER_0)

/**
 * @brief How often should be PWM value stored in NVS
 */
#define PWR_LED_STORE_PWM_VALUE_IN_NVM_S (30)
// ===============================| Advanced |================================

// =========================| Function like macros |==========================
/**
 * @brief Define function which returns ms (32 bit value) as time reference
 */
#define GET_CLK_MS_32BIT() esp_log_timestamp()

/**
 * @brief Delay procedure
 *
 * @param delay_ms Delay in ms
 *
 * @note Using something already solved by "sensors" module
 *
 */
#define DELAY_MS(delay_ms) sensor_delay_ms(delay_ms)

// ============================| Default values |=============================
/**
 * @brief When enabled, it shows stack usage
 */
#define SHOW_STACK_USAGE (0)
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
#endif  // __CFG_H__
