# DC/DC

* Used IC `MT7282`
* Efficiency was at about 67% at 5 V @ 1.6 A (5 white LED in serial)
* Output current was 0.28 A @ ~16 V
* Overheating caused emergency power off of DC/DC controller

# Resistor

* Assume 5V input, 3V drop on LED, 0.28 A -> 60%