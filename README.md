# Steampunk lamp with ESP32
 * The idea is to have cool lamp with set of sensors which can do some
   "magic" stuff. Increase or decrease light intensity even without any touch!

 ![avatar](doc/avatar.jpg)

 ![function demo video](doc/function_demo.mp4)

* Heart of device is good old [ESP32](https://www.espressif.com/en/products/socs/esp32),
  which provides enough firepower for good price
* Previous version is based on *ATTiny13*, but due to low ROM space,
  functionality is quite limited, although it worked fair (analog IR sensors)
* Current version is using digital IR sensors and therefore it allows to
  control this lamp via any remote

# HW
 * Project is designed in [KiCad](https://www.kicad.org/), however all data was
   exported into [hw/export](hw/export/).
 * You need to pay attention to the [power line](hw/export/power_line_sch.pdf).
   In short, high currents should not float through control PCB. Only exception
   is MOS-FET, which is working as switch for PWM. Other than that, even
   grounding should have own wires.
 * Additional electrolytic capacitor might be necessary in order to compensate
   inductance and resistance on the power cables. In short: it is necessary to
   have stable voltage for LEDs, otherwise you might see some flickering.
 * During testing it turns out that using simple resistors is better solution
   than using DC/DC for driving LEDs. The efficiency does not differ so much
   and resistors can be easily placed on the outer side of lamp while having
   good heat dissipation. Plus resistors will not "turn off" when overheating.

# FW
 * [Download binaries](https://gitlab.com/martin.stejskal/steampunk-lamp-with-esp32/-/releases)
 * Just upload it into MCU and see miracles.
 * Default settings should work out of the box. If not, use *service UI*
   (UART 115200 - *User_interface* connector) to adjust parameters. Simply
   connect and USB/UART converter and hit *enter* key. Device will show you
   detailed help. Via this interface you can easily reconfigure device without
   need of recompiling code.
 * Device have temperature protection. If external sensor is used (MPU9250 or
   similar), device can detect high temperature and reduce light intensity in
   order to prolong life of LEDs and other components.
 * If magnetometer sensor is used, it is possible to regulate light intensity
   by rotating magnet. Moreover, if you remove magnet, it will "power off"
   device and when magnet will be placed back, it will "power on" device.
 * Most of this overengineering stuff is there just to "play" with sensors :)

# Photos

![1](doc/foto/2022.04.11_19.27.41.jpg)

![2](doc/foto/2022.04.11_19.30.42.jpg)

![3](doc/foto/2022.04.11_19.35.56.jpg)

![4](doc/foto/2022.04.11_19.36.31.jpg)

![5](doc/foto/2022.04.11_19.36.48.jpg)

![6](doc/foto/2022.04.11_19.39.34.jpg)

![7](doc/foto/2022.04.11_19.40.07.jpg)

![8](doc/foto/2022.04.11_19.47.01.jpg)
